import { Button } from "react-bootstrap";
import { Component } from "react";
import AddWorkExperienceModal from "./WorkExperience/AddWorkExperienceModal";
import NotificationToast from "./components/NotificationToast";
import EditWorkExperienceModal from "./WorkExperience/EditWorkExperienceModal";
import AddEducationModal from "./Education/AddEducationModal";
import EditEducationModal from "./Education/EditEducationModal";
import PersonalIdentityModal from "./PersonalIdentity/PersonalIdentityModal";

class TesProfile extends Component{
    constructor(props){
        super(props);
        console.log(props);
        this.state = {
            showModals: {
                addWorkExperience: false,
                editWorkExperience: false,
                addEducation: false,
                editEducation: false,
            },
            userId: props.match.params.userId,
            notificationToast: [],
        };
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }
    handleShowModal(e){
        const modalName = e.target.getAttribute('modalName');
        this.setState(prevState => ({
            showModals: {
                ...prevState.showModals, [modalName]: true
            }

        }));
    }
    handleCloseModal(message, modalName){
        let {notificationToast} = this.state;
        const toastId = `${Math.random().toString(36).substr(2, 9)}-${modalName}`;
        if(message){
            notificationToast.push(
                <NotificationToast 
                    id={toastId} 
                    key={toastId} 
                    header="Berhasil!" body={message} show={true} 
                    onClose={() => {
                        let {notificationToast} = this.state;
                        notificationToast = notificationToast.filter((toast) => {console.log(toastId); return toast.props.id !== toastId});
                        this.setState({
                            notificationToast
                        })
                    }}
                />
            );
            this.setState(prevState => ({
                notificationToast,
                showModals: {
                    ...prevState.showModals, [modalName]: false
                }
            }));
        }else{
            this.setState(prevState => ({
                showModals: {
                    ...prevState.showModals, [modalName]: false
                }
            }));

        }
    }
    render(){
        const {
            notificationToast,
            showModals,
            userId
        } = this.state;

        return (
            <>
                {notificationToast}
                <Button variant="primary" modalName="personalIdentity" onClick={this.handleShowModal}>
                    Personal Identity
                </Button>
                <PersonalIdentityModal  
                    name="personalIdentity"
                    id={userId}
                    show={showModals.personalIdentity}
                    handleClose={(message) => this.handleCloseModal(message, "personalIdentity")}
                    getToken={this.props.getToken}
                />
                <Button variant="primary" modalName="addWorkExperience" onClick={this.handleShowModal}>
                    Add Work Experience
                </Button>
                <AddWorkExperienceModal  
                    name="addWorkExperience"
                    show={showModals.addWorkExperience}
                    handleClose={(message) => this.handleCloseModal(message, "addWorkExperience")}
                    getToken={this.props.getToken}
                />
                <Button variant="primary" modalName="editWorkExperience" onClick={this.handleShowModal}>
                    Edit Work Experience
                </Button>
                <EditWorkExperienceModal  
                    id="b97eeac6-5aa4-4d73-94f5-e30d50100aad"
                    name="editWorkExperience"
                    show={showModals.editWorkExperience}
                    handleClose={(message) => this.handleCloseModal(message, "editWorkExperience")}
                    getToken={this.props.getToken}
                />

                <Button variant="primary" modalName="addEducation" onClick={this.handleShowModal}>
                    Add Education
                </Button>
                <AddEducationModal 
                    name="addEducation"
                    show={showModals.addEducation}
                    handleClose={(message) => this.handleCloseModal(message, "addEducation")}
                    getToken={this.props.getToken}
                />
                <Button variant="primary" modalName="editEducation" onClick={this.handleShowModal}>
                    Edit Education
                </Button>
                <EditEducationModal 
                    name="editEducation"
                    id="ec6cc510-24d5-4d3e-9587-76ca34c533cf"
                    show={showModals.editEducation}
                    handleClose={(message) => this.handleCloseModal(message, "editEducation")}
                    getToken={this.props.getToken}
                />
            </>
        );
    }
}

export default TesProfile;