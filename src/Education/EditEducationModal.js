import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
    InputGroup
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import {API_URL} from "../constants";
import axios from "axios";

class EditEducationModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            degrees: [],
            educationId: props.id,
            formData: {},
            isLoaded: false,
            showModal: false
        };

        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            if (this.state.errors.schoolName) {
                if (name === "schoolName") {
                    delete this.state.errors['schoolName'];
                }
            }

            if (this.state.errors.degreeId) {
                if (name === "degreeId") {
                    delete this.state.errors['degreeId'];
                }
            }
        }

        this.setState(prevState => ({
            formData:{
                ...prevState.formData, [name]: value
            }
        }));
    }
    handleChangeStartDate(startYear){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, startYear
            }
        }));
    }

    async handleChangeEndDate(endYear){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, endYear
            }
        }));
    }

    handleShowModal(event) {
        this.setState({
            showModal: true
        });
    }

    handleCloseModal() {
        this.setState({
            showModal: false
        });
    }
    
    handleSave(){
        //call api untuk menambahkan pendidikan
        
        const {formData, educationId} = this.state;
        
        if (formData.degreeId === "-") {
            formData.degreeId = "";
        }

        if(formData.startYear) {
            formData.startYear = new Date(formData.startYear).getFullYear();
        }  else {
            formData.startYear = null
        }

        if(formData.endYear) {
            formData.endYear = new Date(formData.endYear).getFullYear();
        } else {
            formData.endYear = null;
        }

        const token = this.props.getToken();

        axios.put(API_URL + `educations/${educationId}`, formData, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            if (response.data.result.errors) {
                this.setState({
                    errors: response.data.result.errors
                })

                if (this.state.formData.startYear) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            startYear: new Date(this.state.formData.startYear + 1, 0, 0)
                        }
                    }))
                }

                if (this.state.formData.endYear) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            endYear: new Date(this.state.formData.endYear + 1, 0, 0)
                        }
                    }))
                }
            } else {
                this.setState({
                    formData: {}
                });

                this.props.handleClose("Data pendidikan berhasil ditambahkan.");

                window.location.reload();
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
    handleDelete(){
        //call api untuk menambahkan pendidikan
        const {educationId} = this.state;

        const token = this.props.getToken();
        axios.delete(API_URL + `educations/${educationId}`, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                formData: {}
            });
            this.props.handleClose("Data pendidikan berhasil dihapus.");
            window.location.reload();
        })
        .catch(error => {
            console.log(error);
        })
    }

    getDegrees(){
        const token = this.props.getToken();
        axios.get(API_URL + 'degrees', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                degrees: response.data.result,
                isLoaded: true
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    getEducationData(){
        const token = this.props.getToken();
        const id = this.state.educationId;
        axios.get(API_URL + `educations/${id}`, {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            const result = response.data.result;

            result.degreeId = result.degree.name

            if (result.startYear) {
                result.startYear = new Date(result.startYear + 1, 0, 0);
            }
            if (result.endYear) {
                result.endYear = new Date(result.endYear + 1, 0, 0);
            }

            delete result['userId'];
            delete result['degree'];

            this.setState({
                formData: result
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    componentDidMount(){
        if(this.state.degrees.length === 0 && !this.state.isLoaded){
            this.getDegrees();
        }
        if(this.state.educationId && this.props.show){
            this.getEducationData();
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {
            this.setState({
                educationId: this.props.id
            }, this.getEducationData())
        }
    }
    render(){
        const {
            schoolName, 
            degreeId, 
            fieldOfStudy,
            startYear, 
            endYear,
        } = this.state.formData;
        const {
            degrees,
            showModal,
            errors
        } = this.state;
        const {props} = this;

        if (showModal) {
            return (
                <Modal show={showModal} onHide={this.handleCloseModal}>
                    <Modal.Header closeButton>Hapus pendidikan</Modal.Header>
                    <Modal.Body>Apakah Anda yakin Anda ingin menghapus pendidikan ini?</Modal.Body>
                    <Modal.Footer>
                        <Button className="cancel-button rounded-24 border-2 bg-white text-1 font-weight-semi-bold" onClick={this.handleCloseModal}>Batalkan</Button>
                        <Button className="delete-button rounded-24 bg-3 font-weight-semi-bold" onClick={this.handleDelete}>Hapus</Button>
                    </Modal.Footer>
                </Modal>
            )
        } else {
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={() => {delete this.state['errors']; props.handleClose()}}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Pendidikan</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                        <Form>
                            <Form.Group className="mb-3">
                                <Form.Label>Sekolah *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Control value={schoolName} name="schoolName" type="text" placeholder="Mis: Politeknik Negeri Bandung" onChange={this.handleChange} isInvalid={errors? (typeof errors.schoolName === "undefined"? false : errors.schoolName) : false}/>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors? (typeof errors.schoolName === "undefined"? "" : errors.schoolName) : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
    
                            <Form.Group className="mb-3">
                                <Form.Label>Gelar *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Select aria-label="-" name="degreeId" onChange={this.handleChange} value={degreeId} isInvalid={errors? (typeof errors.degreeId === "undefined"? false : errors.degreeId) : false}>
                                        <option>-</option>
                                        { degrees && degrees.map((item) => {
                                            return (
                                                <option key={item.id} value={item.id}>{item.name}</option>
                                            )
                                        })
    
                                        }
                                    </Form.Select>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors? (typeof errors.degreeId === "undefined"? "" : errors.degreeId) : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
    
                            <Form.Group className="mb-3">
                                <Form.Label>Bidang Studi</Form.Label>
                                <Form.Control value={fieldOfStudy} name="fieldOfStudy" type="text" placeholder="Mis: Teknik Informatika" onChange={this.handleChange}/>
                            </Form.Group>
    
                            <Row>
                                <Col sm={12} md={6}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Tahun Mulai *</Form.Label>
                                        <Row>
                                            <Col>
                                                <DatePicker
                                                    selected={startYear}
                                                    name="startYear"
                                                    onChange={this.handleChangeStartDate}
                                                    dateFormat="yyyy"
                                                    placeholderText="Tahun"
                                                    showYearPicker
                                                />
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Col>
                                <Col sm={12} md={6}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Tahun Berakhir</Form.Label>
                                        <Row>
                                            <Col>
                                                <DatePicker
                                                    selected={endYear}
                                                    name="endYear"
                                                    onChange={this.handleChangeEndDate}
                                                    dateFormat="yyyy"
                                                    placeholderText="Tahun"
                                                    showYearPicker
                                                />
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Col>
                            </Row>
    
                        </Form>
                        </Modal.Body>
    
                        <Modal.Footer>
                            <div className="mr-auto">
                                <Button className="button-pill" variant="outline-primary"onClick={this.handleShowModal}>Hapus</Button>
                            </div>
                            <div className="ml-auto">
                                <Button className="button-pill" variant="primary" onClick={this.handleSave}>Simpan</Button>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </>
            );
        }
        
    }
}

export default EditEducationModal;