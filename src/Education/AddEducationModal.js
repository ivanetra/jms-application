import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
    InputGroup
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import axios from "axios";
import {API_URL} from "../constants";

class AddEducationModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            degrees: [],
            formData: {},
            isLoaded: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            if (this.state.errors.schoolName) {
                if (name === "schoolName") {
                    delete this.state.errors['schoolName'];
                }
            }

            if (this.state.errors.degreeId) {
                if (name === "degreeId") {
                    delete this.state.errors['degreeId'];
                }
            }
        }

        this.setState(prevState => ({
            formData:{
                ...prevState.formData, [name]: value
            }
        }));
    }

    handleChangeStartDate(startYear){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, startYear
            }
        }));
    }
    handleChangeEndDate(endYear){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, endYear
            }
        }));
    }
    
    handleSave(){
        //call api untuk menambahkan pendidikan
        const {formData} = this.state;

        if (formData.degreeId === "-") {
            formData.degreeId = "";
        }

        if(formData.startYear) {
            formData.startYear = new Date(formData.startYear).getFullYear();
        }  else {
            formData.startYear = null
        }

        if(formData.endYear) {
            formData.endYear = new Date(formData.endYear).getFullYear();
        } else {
            formData.endYear = null;
        }

        const token = this.props.getToken();

        axios.post(API_URL + 'educations', formData, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            if (response.data.result.errors) {
                this.setState({
                    errors: response.data.result.errors
                })

                if (this.state.formData.startYear) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            startYear: new Date(this.state.formData.startYear + 1, 0, 0)
                        }
                    }))
                }

                if (this.state.formData.endYear) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            endYear: new Date(this.state.formData.endYear + 1, 0, 0)
                        }
                    }))
                }
    
            } else {
                this.setState({
                    formData: {}
                });

                this.props.handleClose("Data pendidikan berhasil ditambahkan.");

                window.location.reload();
            }
        })
        .catch(error => {
            this.setState({
                errors: error.response.data
            })

            if (this.state.formData.startYear) {
                this.setState(prevState => ({
                    formData: {
                        ...prevState.formData,
                        startYear: new Date(this.state.formData.startYear + 1, 0, 0)
                    }
                }))
            }

            if (this.state.formData.endYear) {
                this.setState(prevState => ({
                    formData: {
                        ...prevState.formData,
                        endYear: new Date(this.state.formData.endYear + 1, 0, 0)
                    }
                }))
            }
        })
    }

    getDegrees(){
        const token = this.props.getToken();
        axios.get(API_URL + 'degrees', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                degrees: response.data.result,
                isLoaded: true
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    componentDidMount(){
        if(this.state.degrees.length === 0 && !this.state.isLoaded){
            this.getDegrees();
        }
    }

    render(){
        const {
            schoolName, 
            degreeId, 
            fieldOfStudy,
            startYear, 
            endYear,
        } = this.state.formData;
        const {
            degrees,
            errors
        } = this.state;
        const {props} = this;
        return (
            <>                
                <Modal
                    show={props.show}
                    onHide={() => {delete this.state['errors']; this.setState({formData: {}}); props.handleClose()}}
                    size="lg"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Pendidikan</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Sekolah *</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Control value={schoolName} name="schoolName" type="text" placeholder="Mis: Politeknik Negeri Bandung" onChange={this.handleChange} isInvalid={errors? (typeof errors.schoolName === "undefined"? false : errors.schoolName) : false}/>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors? (typeof errors.schoolName === "undefined"? "" : errors.schoolName) : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Gelar *</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Select aria-label="-" name="degreeId" onChange={this.handleChange} value={degreeId} isInvalid={errors? (typeof errors.degreeId === "undefined"? false : errors.degreeId) : false}>
                                    <option>-</option>
                                    { degrees && degrees.map((item) => {
                                        return (
                                            <option key={item.name} value={item.name}>{item.name}</option>
                                        )
                                    })

                                    }
                                </Form.Select>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors? (typeof errors.degreeId === "undefined"? "" : errors.degreeId) : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Bidang Studi</Form.Label>
                            <Form.Control value={fieldOfStudy} name="fieldOfStudy" type="text" placeholder="Mis: Teknik Informatika" onChange={this.handleChange}/>
                        </Form.Group>

                        <Row>
                            <Col sm={12} md={6}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Tahun Mulai *</Form.Label>
                                    <Row>
                                        <Col>
                                            <DatePicker
                                                selected={startYear}
                                                name="startYear"
                                                onChange={this.handleChangeStartDate}
                                                dateFormat="yyyy"
                                                placeholderText="Tahun"
                                                showYearPicker
                                            />
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={6}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Tahun Berakhir</Form.Label>
                                    <Row>
                                        <Col>
                                            <DatePicker
                                                selected={endYear}
                                                name="endYear"
                                                onChange={this.handleChangeEndDate}
                                                dateFormat="yyyy"
                                                placeholderText="Tahun"
                                                showYearPicker
                                            />
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Col>
                        </Row>

                    </Form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button className="button-pill" variant="primary" onClick={this.handleSave}>Simpan</Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default AddEducationModal;