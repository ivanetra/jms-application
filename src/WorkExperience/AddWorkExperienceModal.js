import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
    InputGroup
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import {API_URL} from "../constants";
import axios from "axios";

class AddWorkExperienceModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            workExperienceTypes: [],
            formData: {},
            isLoaded: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            if (this.state.errors.title) {
                if (name === "title") {
                    delete this.state.errors['title'];
                }
            }

            if (this.state.errors['workExperienceType.name']) {
                if (name === "workExperienceType") {
                    delete this.state.errors['workExperienceType.name'];
                }
            }

            if (this.state.errors.companyName) {
                if (name === "companyName") {
                    delete this.state.errors['companyName'];
                }
            }
        }
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, [name]: value
            }
        }));
    }

    handleChangeStartDate(startDate){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, startDate
            }
        }));
    }
    
    handleChangeEndDate(endDate){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, endDate
            }
        }));
    }
    
    handleSave(){
        //call api untuk menambahkan pengalaman kerja
        const {formData} = this.state;
        
        if (formData.workExperienceType === "-") {
            formData.workExperienceType = ""
        }

        if (formData.startDate) {
            formData.startDate = new Date(formData.startDate);
        } else {
            formData.startDate = ""
        }

        if (formData.endDate) {
            formData.endDate = new Date(formData.endDate);
        } else {
            formData.endDate = ""
        }

        const token = this.props.getToken();
        axios.post(API_URL + 'work-experience', formData, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            const result = response.data.result;
            
            if (result.errors) {
                this.setState({
                    errors: result.errors
                })
            } else {
                this.setState({
                    formData: {}
                });

                this.props.handleClose("Data pengalaman kerja berhasil ditambahkan.");

                window.location.reload();
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    getWorkExperienceTypes(){
        const token = this.props.getToken();
        axios.get(API_URL + 'work-experience-type', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                workExperienceTypes: response.data.result,
                isLoaded: true
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    componentDidMount(){
        if(this.state.workExperienceTypes.length === 0 && !this.state.isLoaded){
            this.getWorkExperienceTypes();
        }
    }

    render(){
        const {
            title, 
            workExperienceType, 
            companyName,
            startDate, 
            endDate,
        } = this.state.formData;
        const {
            workExperienceTypes,
            errors
        } = this.state;
        const {props} = this;
        return (
            <>                
                <Modal
                    show={props.show}
                    onHide={() => {delete this.state['errors']; this.setState({formData: {}}); props.handleClose()}}
                    size="lg"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Pengalaman</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Judul *</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Control value={title} name="title" type="text" placeholder="Mis: Software Engineer" onChange={this.handleChange} isInvalid={errors? (typeof errors.title === "undefined"? false : errors.title) : false}/>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors? (typeof errors.title === "undefined"? "" : errors.title) : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Jenis Pekerjaan *</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Select aria-label="-" name="workExperienceType" onChange={this.handleChange} value={workExperienceType} isInvalid={errors? (typeof errors['workExperienceType.name'] === "undefined"? false : errors['workExperienceType.name']) : false}>
                                    <option>-</option>
                                    { workExperienceTypes && workExperienceTypes.map((item) => {
                                        return (
                                            <option key={item.name} value={item.name}>{item.name}</option>
                                        )
                                    })

                                    }
                                </Form.Select>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors ? errors['workExperienceType.name'] : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Perusahaan *</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Control value={companyName} name="companyName" type="text" placeholder="Mis: Perusahaan 1" onChange={this.handleChange} isInvalid={errors? (typeof errors.companyName === "undefined"? false : errors.companyName) : false}/>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors ? errors.companyName : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                            
                        </Form.Group>

                        {/* <Form.Group className="mb-3">
                            <Form.Label>Lokasi *</Form.Label>
                            <Form.Control name="location" type="text" placeholder="Mis: Bandung, Indonesia" onChange={this.handleChange}/>
                        </Form.Group> */}

                        <Row>
                            <Col sm={12} md={6}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Tanggal Mulai *</Form.Label>
                                    <Row>
                                        <Col className="mb-2">
                                            <DatePicker
                                                selected={startDate}
                                                name="startDate"
                                                onChange={this.handleChangeStartDate}
                                                dateFormat="MMMM yyyy"
                                                showMonthYearPicker
                                            />
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={6}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Tanggal Selesai</Form.Label>
                                    <Row>
                                        <Col className="mb-2">
                                            <DatePicker
                                                selected={endDate}
                                                name="endDate"
                                                onChange={this.handleChangeEndDate}
                                                dateFormat="MMMM yyyy"
                                                showMonthYearPicker
                                            />
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Col>
                        </Row>

                    </Form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button className="button-pill" variant="primary" onClick={this.handleSave}>Simpan</Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default AddWorkExperienceModal;