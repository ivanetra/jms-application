import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
    InputGroup
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import {API_URL} from "../constants";
import axios from "axios";

class EditWorkExperienceModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            workExperienceTypes: [],
            workExperienceId: props.id,
            formData: {},
            isLoaded: false,
            showModals: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeStartDate= this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;


        if (this.state.errors) {
            if (this.state.errors.title) {
                if (name === "title") {
                    delete this.state.errors['title'];
                }
            }

            if (this.state.errors['workExperienceType.name']) {
                if (name === "workExperienceType") {
                    delete this.state.errors['workExperienceType.name'];
                }
            }

            if (this.state.errors.companyName) {
                if (name === "companyName") {
                    delete this.state.errors['companyName'];
                }
            }
        }

        this.setState(prevState => ({
            formData:{
                ...prevState.formData, [name]: value
            }
        }));
    }

    handleChangeEndDate(endDate) {
        const { errors } = this.state;

        if (errors) {
            if (errors.endDate) {
                delete errors['endDate'];
            }
        }

        this.setState(prevState => ({
            formData:{
                ...prevState.formData, endDate
            }
        }));
    }

    handleChangeStartDate(startDate){
        const { errors } = this.state;

        if (errors) {
            if (errors.startDate) {
                delete errors['startDate'];
            }
        }
    
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, startDate
            }
        }));
    }
    
    handleSave(){
        //call api untuk menambahkan pengalaman kerja
        const {formData, workExperienceId} = this.state;

        if (formData.workExperienceType === "-") {
            formData.workExperienceType = ""
        }

        if (formData.startDate) {
            formData.startDate = new Date(formData.startDate);
        } else {
            formData.startDate = ""
        }

        if (formData.endDate) {
            formData.endDate = new Date(formData.endDate);
        } else {
            formData.endDate = ""
        }

        console.log(formData)

        const token = this.props.getToken();

        axios.put(API_URL + `work-experience/${workExperienceId}`, formData, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {  
            console.log(response.data);

            if (response.data.result.errors) {
                this.setState({
                    errors: response.data.result.errors
                })
            } else {
                this.setState({
                    formData: {}
                });
    
                this.props.handleClose("Data pengalaman kerja berhasil diubah.");
                window.location.reload();
            }
        })
        .catch((error) => {
            console.log(error);
        })
    }

    handleShowModal(event) {
        this.setState({
            showModals: true
        });
    }

    handleCloseModal() {
        this.setState({
            showModals: false
        });
    }

    handleDelete(){
        //call api untuk menambahkan pengalaman kerja
        const {workExperienceId} = this.state;

        const token = this.props.getToken();
        axios.delete(API_URL + `work-experience/${workExperienceId}`, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response.data);
            this.setState({
                formData: {}
            });
            this.props.handleClose("Data pengalaman kerja berhasil dihapus.");
            window.location.reload();
        })
        .catch(error => {
            console.log(error);
        })
    }

    getWorkExperienceTypes(){
        const token = this.props.getToken();
        axios.get(API_URL + 'work-experience-type', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                workExperienceTypes: response.data.result,
                isLoaded: true
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    getWorkExperienceData(){
        const token = this.props.getToken();
        const id = this.state.workExperienceId;
        axios.get(API_URL + `work-experience/${id}`, {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response)
            const formData = response.data.result;

            formData.workExperienceType = formData.workExperienceType.name

            if (formData.startDate) {
                formData.startDate = new Date(formData.startDate);
            }
            if (formData.endDate) {
                formData.endDate = new Date(formData.endDate);
            }

            delete formData['userId'];

            this.setState({
                formData
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    componentDidMount(){
        if(this.state.workExperienceTypes.length === 0 && !this.state.isLoaded){
            this.getWorkExperienceTypes();
        }
        if(this.state.workExperienceId && this.props.show){
            this.getWorkExperienceData();
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {
            this.setState({
                workExperienceId: this.props.id
            }, this.getWorkExperienceData())
        }
    }
    render(){
        const {
            title, 
            workExperienceType, 
            companyName,
            startDate,
            endDate
        } = this.state.formData;

        // if(!(startDate instanceof Date)) startDate = new Date(startDate);
        // if(!(endDate instanceof Date)) endDate = new Date(endDate);
        const {
            workExperienceTypes,
            showModals,
            errors
        } = this.state;
        const {props} = this;

        if (showModals) {
            return (
                <Modal show={showModals} onHide={this.handleCloseModal}>
                    <Modal.Header closeButton>Hapus pengalaman</Modal.Header>
                    <Modal.Body>Apakah Anda yakin Anda ingin menghapus pengalaman ini?</Modal.Body>
                    <Modal.Footer>
                        <Button className="cancel-button rounded-24 border-2 bg-white text-1 font-weight-semi-bold" onClick={this.handleCloseModal}>Batalkan</Button>
                        <Button className="delete-button rounded-24 bg-3 font-weight-semi-bold" onClick={this.handleDelete}>Hapus</Button>
                    </Modal.Footer>
                </Modal>
            )
        } else {
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={() => {delete this.state['errors']; props.handleClose()}}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Pengalaman</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                        <Form>
                            <Form.Group className="mb-3">
                                <Form.Label>Judul *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Control value={title} name="title" type="text" placeholder="Mis: Software Engineer" onChange={this.handleChange} isInvalid={errors? (typeof errors.title === "undefined"? false : errors.title) : false}/>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors? (typeof errors.title === "undefined"? "" : errors.title) : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
    
                            <Form.Group className="mb-3">
                                <Form.Label>Jenis Pekerjaan *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Select aria-label="-" name="workExperienceType" onChange={this.handleChange} value={workExperienceType} isInvalid={errors? (typeof errors['workExperienceType.name'] === "undefined"? false : errors['workExperienceType.name']) : false}>
                                        <option>-</option>
                                        { workExperienceTypes && workExperienceTypes.map((item) => {
                                            return (
                                                <option key={item.name} value={item.name}>{item.name}</option>
                                            )
                                        })}
                                    </Form.Select>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors['workExperienceType.name'] : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Perusahaan *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Control value={companyName} name="companyName" type="text" placeholder="Mis: Perusahaan 1" onChange={this.handleChange} isInvalid={errors? (typeof errors.companyName === "undefined"? false : errors.companyName) : false}/>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors.companyName : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
    
    
                            <Row>
                                <Col sm={12} md={6}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Tanggal Mulai *</Form.Label>
                                        <Row>
                                            <Col className="mb-2">
                                                <DatePicker
                                                    selected={startDate}
                                                    name="startMonth"
                                                    onChange={this.handleChangeStartDate}
                                                    dateFormat="MMMM yyyy"
                                                    showMonthYearPicker
                                                />
                                                {errors?
                                                    [typeof errors.startDate === "undefined"?
                                                        null 
                                                    :
                                                        <div className="mt-2 text-danger font-size-14">
                                                            {errors.startDate}
                                                        </div>
                                                    ]
                                                :
                                                    null
                                                }
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Col>
                                <Col sm={12} md={6}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Tanggal Selesai</Form.Label>
                                        <Row>
                                            <Col className="mb-2">
                                                <DatePicker
                                                    selected={endDate}
                                                    name="endDate"
                                                    onChange={this.handleChangeEndDate}
                                                    dateFormat="MMMM yyyy"
                                                    placeholderText="Bulan"
                                                    showMonthYearPicker
                                                />
                                                {errors?
                                                    [typeof errors.endDate === "undefined"?
                                                        null 
                                                    :
                                                        <div className="mt-2 text-danger font-size-14">
                                                            {errors.endDate}
                                                        </div>
                                                    ]
                                                :
                                                    null
                                                }
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </Form>
                        </Modal.Body>
    
                        <Modal.Footer>
                            <div className="mr-auto">
                                <Button className="button-pill" variant="outline-primary" onClick={this.handleShowModal}>Hapus</Button>
                            </div>
                            <div className="ml-auto">
                                <Button className="button-pill" variant="primary" onClick={this.handleSave}>Simpan</Button>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </>
            );
        }
        
    }
}

export default EditWorkExperienceModal;