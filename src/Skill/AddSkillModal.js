import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
} from "react-bootstrap";
import {API_URL} from "../constants";
import axios from "axios";
import AsyncSelect from 'react-select/async';
import PropTypes from 'prop-types';

class AddSkillModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            formData: {
                skillId: []
            },
            isLoaded: false,
            inputValue: ""
        };
        this.handleSave = this.handleSave.bind(this);
        this.loadOptions = this.loadOptions.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    handleChange(skills) {
        this.setState({
            formData: {
                skillId: skills
            }
        })
    }

    handleInputChange(inputValue){
        this.setState({
            inputValue: inputValue
        });
    }

    async loadOptions(inputValue){
        const token = this.props.getToken();

        if (this.state.inputValue) {
            const { data } = await axios.get(API_URL + 'skills/search?skillQuery=' + inputValue, {
                headers: {
                accept: "application/json",
                authorization: "Bearer " + token
                }
            })
            .catch(error => {
                console.log(error);
            })
    
            const options = data.result.map((skill) => ({
                label: skill.name,
                value: skill.id
            }))

            console.log(options)
    
            return options;
        }
    }

    handleChangeStartDate(startDate){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, startDate
            }
        }));
    }
    handleChangeEndDate(endDate){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, endDate
            }
        }));
    }
    
    handleSave(){
        //call api untuk menambahkan keahlian
        const {formData} = this.state;

        formData.skillId.map(function(skill, index) { 
            delete skill.label; 
            formData.skillId[index] = Object.values(skill);
            return formData
        });

        const token = this.props.getToken();
        const nim = this.props.getNim();

        axios.post(API_URL + 'users/' + nim + '/skills/add', formData, {
            headers: {
                accept: "applcation/json",
                contentType: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            const data = response.data;

            console.log(data);

            if (data.status) {
                this.setState({
                    formData: {}
                });

                this.props.handleClose("Data keahlian berhasil ditambahkan.");

                window.location.reload();
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    getWorkExperienceTypes(){
        const token = this.props.getToken();
        axios.get(API_URL + 'work-experience-type', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                workExperienceTypes: response.data.result,
                isLoaded: true
            });
        })
        .catch(error => {
            console.log(error);
        })
    }

    render(){
        const {props} = this;
        return (
            <>                
                <Modal
                    show={props.show}
                    onHide={props.handleClose}
                    size="lg"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Keahlian</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                        <AsyncSelect
                            isMulti
                            cacheOptions
                            defaultOptions
                            loadOptions={this.loadOptions}
                            onInputChange={this.handleInputChange}
                            onChange={this.handleChange}
                            noOptionsMessage={() => null}
                            placeholder="Keahlian (mis: Software Engineering)"
                            value={this.state.formData.skillId}
                        />
                        </Form.Group>
                    </Form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button className="button-pill" variant="primary" onClick={this.handleSave}>Tambah  </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

AddSkillModal.propTypes = {
    getNim: PropTypes.func.isRequired
}

export default AddSkillModal;