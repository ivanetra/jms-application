import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from 'axios';
import PropTypes from 'prop-types';
import moment from 'moment';
import PersonalIdentityModal from "./PersonalIdentity/PersonalIdentityModal";
import NotificationToast from "./components/NotificationToast";
import ReactRoundedImage from "react-rounded-image";
import AddWorkExperienceModal from "./WorkExperience/AddWorkExperienceModal";
import EditWorkExperienceModal from "./WorkExperience/EditWorkExperienceModal";
import AddEducationModal from "./Education/AddEducationModal";
import AddSkillModal from "./Skill/AddSkillModal";
import EditEducationModal from "./Education/EditEducationModal";
import {API_URL} from "./constants";
import { Modal, Button } from "react-bootstrap";
 
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: props.match.params.userId,
            user: {},
            isLoaded: false,
            showModals: {
                addWorkExperience: false,
                editWorkExperience: [false],
                addEducation: false,
                editEducation: [false],
                addSkill: false,
                deleteSkill: []
            },
            notificationToast: []
        };
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        const token = this.props.getToken();

        axios.get("https://api.jobmatcher.me/v1/users/" + this.state.userId, {
            headers: {
                accept: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }

            let user = response.data.user;

            return axios.get("https://api.jobmatcher.me/v1/work-experience?userId=" + this.state.userId, {
                headers: {
                    accept: "application/json",
                    authorization: "Bearer " + token
                }
            })
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }

                user.experiences = response.data.result;

                let editWorkExperienceModals = [];

                user.experiences.map(experience => {
                    return editWorkExperienceModals.push(false);
                })

                    return axios.get("https://api.jobmatcher.me/v1/educations?userId=" + this.state.userId, {
                        headers: {
                            accept: "application/json",
                            authorization: "Bearer " + token
                        }
                    })
                    .then(response => {
                        if (response.status >= 400) {
                            throw new Error("Bad response from server");
                        }
                
                        user.education = response.data.result;

                        let editEducationModals = [];

                        user.education.map(education => {
                            return editEducationModals.push(false);
                        })

                        return axios.get("https://api.jobmatcher.me/v1/users/" + this.state.userId + "/skills", {
                            headers: {
                                accept: "application/json",
                                authorization: "Bearer " + token
                            }
                        })
                        .then(response => {
                            if (response.status >= 400) {
                                throw new Error("Bad response from server");
                            }
                    
                            user.skills = response.data.userSkills;

                            let deleteSkillModals = [];

                            user.skills.map(() => {
                                return deleteSkillModals.push(false);
                            })

                            this.setState(prevState => ({
                                showModals: {
                                    ...prevState.showModals,
                                    editWorkExperience: editWorkExperienceModals,
                                    editEducation: editEducationModals,
                                    deleteSkill: deleteSkillModals
                                }
                            }));
                            
                            return user;
                        })
                        .then(user => this.setState({
                            user: user,
                            isLoaded: true
                        }))
                    })
            })
        })
        .catch(err => {
            console.log(err);
        });
    }

    handleDelete(e){
        //call api untuk menghapus keahlian
        const nim = this.props.getNim();
        const token = this.props.getToken();
        const formData = {
            skillId: e.currentTarget.getAttribute('skillid')
        }

        axios.post(API_URL + `users/${nim}/skills/remove`, formData,{
            headers: {
                accept: "*/*",
                contentType: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            window.location.reload();
        })
        .catch(error => {
            console.log(error);
        })
    }

    handleCloseModal(message, modalName, key){
        let {notificationToast} = this.state;
        const toastId = `${Math.random().toString(36).substr(2, 9)}-${modalName}`;
        if(message){
            notificationToast.push(
                <NotificationToast 
                    id={toastId} 
                    key={toastId} 
                    header="Berhasil!" body={message} show={true} 
                    onClose={() => {
                        let {notificationToast} = this.state;
                        notificationToast = notificationToast.filter((toast) => {console.log(toastId); return toast.props.id !== toastId});
                        this.setState({
                            notificationToast
                        })
                    }}
                />
            );
            if (key) {
                this.setState(prevState => ({
                    notificationToast,
                    showModals: {
                        ...prevState.showModals,
                        deleteSkill: {
                            [key]: false
                        }
                    }
                }));
            } else {
                this.setState(prevState => ({
                    notificationToast,
                    showModals: {
                        ...prevState.showModals, [modalName]: false
                    }
                }));
            }
        }else{
            if (key) {
                this.setState(prevState => ({
                    notificationToast,
                    showModals: {
                        ...prevState.showModals,
                        deleteSkill: {
                            [key]: false
                        }
                    }
                }));
            } else {
                this.setState(prevState => ({
                    showModals: {
                        ...prevState.showModals, [modalName]: false
                    }
                }));
            }
        }
    }

    handleShowModal(e){
        const modalName = e.currentTarget.getAttribute('modalname');
        const key = e.currentTarget.getAttribute('indexkey');

        if (key) {
            this.setState(prevState => ({
                showModals: {
                    ...prevState.showModals, [modalName]: {
                        [key]: true
                    }
                }
            }));
        } else {
            this.setState(prevState => ({
                showModals: {
                    ...prevState.showModals, [modalName]: true
                }
            }));
        }
    }

    render() {
        const { user, isLoaded, showModals, userId } = this.state;
        if (isLoaded) {
            return (
                <div className="bg-2 flex-grow-1">
                    <div className="container-fluid">
                        <div className="grid row pt-3">
                            <div className="col"></div>
                            <div className="col-xxl-5 col-xl-6">
                                <div className="card w-100 p-4">
                                    <div className="d-flex justify-content-center">
                                        <ReactRoundedImage image={user.photo? user.photo : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="152" imageHeight="152" roundedSize="0" className="card-photo" alt={user.photo? user.name + " profile card" : "Profile card"}/>
                                    </div>
                                    <h1 className="mt-2 mb-0 text-center font-size-24 text-5">{user.name}</h1>
                                    <button className="detail-information btn pt-2 pb-0 bg-white font-size-14 text-1 font-weight-bold" modalname="personalIdentity" onClick={this.handleShowModal}>Informasi detail</button>
                                    <PersonalIdentityModal  
                                        name="personalIdentity"
                                        id={userId}
                                        show={showModals.personalIdentity}
                                        handleClose={(message) => this.handleCloseModal(message, "personalIdentity")}
                                        getToken={this.props.getToken}
                                    />
                                </div>
                                <div className="card flex-column w-100 mt-4 py-4 px-0">
                                    <div className="px-4">
                                        <div className="d-flex flex-row align-items-center w-100">
                                            <h2 className="mb-0 font-size-20 text-5">Pengalaman</h2>
                                            <button className="add-action-button btn ml-auto p-0 bg-white" modalname="addWorkExperience" onClick={this.handleShowModal}>
                                                <img className="add-action" src="/img/Add.svg" alt="Add Action" width="18" height="18"></img>
                                            </button>
                                            <AddWorkExperienceModal  
                                                name="addWorkExperience"
                                                show={showModals.addWorkExperience}
                                                handleClose={(message) => this.handleCloseModal(message, "addWorkExperience")}
                                                getToken={this.props.getToken}
                                            />
                                        </div>
                                        {user.experiences.map((experience, index) => (
                                            <div key={experience.id}>
                                                <div className="d-flex flex-row align-items-center mt-3-5 w-100">
                                                    <h3 className="mb-0 font-size-16 text-5 font-weight-semi-bold">{experience.title}</h3>
                                                    <button className="edit-action-button btn ml-auto p-0 bg-white" modalname="editWorkExperience" indexkey={index} onClick={this.handleShowModal}>
                                                        <img className="edit-action ml-auto" src="/img/Pencil.svg" alt="Edit Action" width="18" height="18"></img>
                                                    </button>
                                                    <EditWorkExperienceModal  
                                                        id={experience.id}
                                                        name="editWorkExperience"
                                                        show={showModals.editWorkExperience[index]}
                                                        handleClose={(message) => this.handleCloseModal(message, "editWorkExperience")}
                                                        getToken={this.props.getToken}
                                                    />
                                                </div>
                                                <div className="d-flex flex-row">
                                                    <p className="company-name mb-0 font-size-14 text-5">{experience.companyName}</p>
                                                    <span className="experience-type font-size-14 text-5">{experience.workExperienceType.name}</span>
                                                </div>
                                                <div className="font-size-14 text-5">
                                                    {moment(new Date(experience.startDate)).format("MMM YYYY")} - {experience.endDate? moment(new Date(experience.endDate)).format("MMM YYYY") : "Saat ini"}
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                    <hr></hr>
                                    <div className="px-4">
                                        <div className="d-flex flex-row align-items-center w-100">
                                            <h2 className="mb-0 font-size-20 text-5">Pendidikan</h2>
                                            <button className="add-action-button btn ml-auto p-0 bg-white" modalname="addEducation" onClick={this.handleShowModal}>
                                                <img className="add-action ml-auto" src="/img/Add.svg" alt="Add Action" width="18" height="18"></img>
                                            </button>
                                            <AddEducationModal 
                                                name="addEducation"
                                                show={showModals.addEducation}
                                                handleClose={(message) => this.handleCloseModal(message, "addEducation")}
                                                getToken={this.props.getToken}
                                            />
                                        </div>
                                        {user.education.map((education, index) => (
                                            <div key={education.id}>
                                                <div className="d-flex flex-row align-items-center w-100 mt-3-5">
                                                    <h3 className="mb-0 font-size-16 text-5 font-weight-semi-bold">{education.schoolName}</h3>
                                                    <button className="edit-action-button btn ml-auto p-0 bg-white" modalname="editEducation" indexkey={index} onClick={this.handleShowModal}>
                                                        <img className="edit-action ml-auto" src="/img/Pencil.svg" alt="Edit Action" width="18" height="18"></img>
                                                    </button>    
                                                    <EditEducationModal 
                                                        name="editEducation"
                                                        id={education.id}
                                                        show={showModals.editEducation[index]}
                                                        handleClose={(message) => this.handleCloseModal(message, "editEducation")}
                                                        getToken={this.props.getToken}
                                                    />                               
                                                </div>
                                                <div className="d-flex flex-row">
                                                    <p className="company-name mb-0 font-size-14 text-5">{education.degree.name}{education.fieldOfStudy? ", " + education.fieldOfStudy : null}</p>
                                                </div>
                                                <div className="font-size-14 text-5">{moment(new Date(education.startYear + 1, 0, 0)).format("YYYY")} - {education.endYear? moment(new Date(education.endYear + 1, 0, 0)).format("YYYY") : "Saat ini"}</div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                <div className="card flex-column w-100 mt-4 pt-4 mb-5 px-0">
                                    <div className="px-4">
                                        <div className="d-flex flex-row align-items-center w-100">
                                            <h2 className="mb-0 font-size-20 text-5">Keahlian</h2>
                                            <button className="add-action-button btn ml-auto p-0 bg-white" modalname="addSkill" onClick={this.handleShowModal}>
                                                <img className="add-action ml-auto" src="/img/Add.svg" alt="Add Action" width="18" height="18"></img>
                                            </button>
                                            <AddSkillModal 
                                                name="addSkill"
                                                show={showModals.addSkill}
                                                handleClose={(message) => this.handleCloseModal(message, "addSkill")}
                                                getToken={this.props.getToken}
                                                getNim={this.props.getNim}
                                            />
                                        </div>
                                        {user.skills.map((skill, index) => (
                                            <div key={skill.id}>
                                                <div className="d-flex flex-row align-items-center w-100">
                                                    <h3 className="mt-3-5 mb-0 font-size-16 text-5 font-weight-semi-bold">{skill.name}</h3>
                                                    <button className="delete-action-button btn ml-auto p-0 bg-white" modalname="deleteSkill" indexkey={index} onClick={this.handleShowModal}>
                                                        <img className="delete-action ml-auto" src="/img/Delete.svg" alt="Delete Action" width="18" height="18"></img>
                                                    </button>
                                                    <Modal show={showModals.deleteSkill[index]} onHide={(message) => this.handleCloseModal(message, "deleteSkill", index)}>
                                                        <Modal.Header closeButton>Hapus keahlian</Modal.Header>
                                                        <Modal.Body>Apakah Anda yakin Anda ingin menghapus keahlian ini?</Modal.Body>
                                                        <Modal.Footer>
                                                            <Button className="cancel-button rounded-24 border-2 bg-white text-1 font-weight-semi-bold" onClick={(message) => this.handleCloseModal(message, "deleteSkill", index)}>Batalkan</Button>
                                                            <Button className="delete-button rounded-24 bg-3 font-weight-semi-bold" skillid={skill.id} onClick={this.handleDelete}>Hapus</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </div>
                                            </div>       
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className="col"></div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div></div>
            );
        }
    }
}

Profile.propTypes = {
    getToken: PropTypes.func.isRequired
}
 
export default withRouter(Profile);