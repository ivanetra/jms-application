import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
    InputGroup
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import {API_URL} from "../constants";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import ReactRoundedImage from "react-rounded-image";

class EditPersonalIdentityModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            userId: props.id,
            religions: [],
            personalIdentity: {
            },
            isLoaded: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeClassYear = this.handleChangeClassYear.bind(this);
        this.handleChangeBirthDate = this.handleChangeBirthDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            if (this.state.errors.name) {
                if (name === "name") {
                    delete this.state.errors['name'];
                }
            }
            if (this.state.errors.gender) {
                if (name === "gender") {
                    delete this.state.errors['gender'];
                }
            }
            if (this.state.errors.religion) {
                if (name === "religion") {
                    delete this.state.errors['religion'];
                }
            }
            if (this.state.errors.phoneNumber) {
                if (name === "phoneNumber") {
                    delete this.state.errors['phoneNumber'];
                }
            }
            if (this.state.errors.studyProgram) {
                if (name === "studyProgram") {
                    delete this.state.errors['studyProgram'];
                }
            }
        }

        if(name === "photo"){
            let file = e.target.files.item(0);
            let reader = new FileReader();
            let url = reader.readAsDataURL(file);

            reader.onloadend = function (e) {
                this.setState((prevState) => ({
                    photoPreview: [reader.result]
                }))
            }.bind(this);

            this.setState(prevState => ({
                personalIdentity:{
                    ...prevState.personalIdentity, photo: file
                }
            }));
        }else{
            if (name === "phoneNumber") {
                if ((/^\d+$/.test(value) || value === "") && value.length <= 11) {
                    this.setState(prevState => ({
                        personalIdentity:{
                            ...prevState.personalIdentity, phoneNumber: value
                        }
                    }));
                }
            } else {
                this.setState(prevState => ({
                    personalIdentity:{
                        ...prevState.personalIdentity, [name]: value
                    }
                }));
            }
        }
    }
    
    handleChangeClassYear(classYear){
        this.setState(prevState => ({
            personalIdentity:{
                ...prevState.personalIdentity, classYear
            }
        }));
    }
    handleChangeBirthDate(birthDate){
        this.setState(prevState => ({
            personalIdentity:{
                ...prevState.personalIdentity, birthDate
            }
        }));
    }
    
    handleSave(){
        const {personalIdentity, userId} = this.state;
        // personalIdentity.studyProgram = Number(personalIdentity.studyProgram);

        if (personalIdentity.gender === "0") {
            delete personalIdentity['gender'];
        }

        if (personalIdentity.religion === "-") {
            delete personalIdentity['religion'];
        }
        
        if (!personalIdentity.birthDate) {
            personalIdentity.birthDate = "";
        }

        if (personalIdentity.classYear) {
            personalIdentity.classYear = personalIdentity.classYear.getFullYear();
        }

        if (personalIdentity.phoneNumber) {
            personalIdentity.phoneNumber = "+62" + personalIdentity.phoneNumber;
        }

        if (personalIdentity.studyProgram === "-") {
            personalIdentity.studyProgram = ""
        }
        
        if(this.state.photoPreview === undefined){
            delete personalIdentity['photo'];
        } else {
            // personalIdentity.photo = this.state.photoPreview[0];
        }

        const token = this.props.getToken();

        let fd = new FormData();
        Object.keys(personalIdentity).forEach((key) => {
            fd.append(key, personalIdentity[key]);
        })

        axios.put(API_URL + `users/${userId}`, fd, {
            headers: {
              contentType: "multipart/form-data",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            if (!response.data.errors) {
                this.setState({
                    personalIdentity: {}
                });
            }
            
            this.props.handleClose("Data diri berhasil diubah.");

            window.location.reload();
        })
        .catch((error) => {
            const {errors} = error.response.data;

            this.setState({
                errors
            })

            if (this.state.personalIdentity.birthDate) {
                this.setState(prevState => ({
                    personalIdentity: {
                        ...prevState.personalIdentity,
                        birthDate: new Date(this.state.personalIdentity.birthDate)
                    }
                }))
            }

            if (this.state.personalIdentity.classYear) {
                this.setState(prevState => ({
                    personalIdentity: {
                        ...prevState.personalIdentity,
                        classYear: new Date(this.state.personalIdentity.classYear + 1, 0, 0)
                    }
                }))
            }

            if (this.state.personalIdentity.phoneNumber) {
                this.setState(prevState => ({
                    personalIdentity: {
                        ...prevState.personalIdentity,
                        phoneNumber: this.state.personalIdentity.phoneNumber.slice(3)
                    }
                }))
            }
        })
    }

    getReligions(){
        const token = this.props.getToken();
        axios.get(API_URL + 'religions', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                religions: response.data.result
            });
        })
        .catch(error => {
            console.log(error);
        })
    }

    getPersonalIdentityData(){
        const token = this.props.getToken();
        const id = this.state.userId;
        axios.get(API_URL + `users/${id}`, {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            const personalIdentity = response.data.user;

            personalIdentity.studyProgram = personalIdentity.studyProgram.id;
            personalIdentity.gender = personalIdentity.gender.id;
            personalIdentity.religion = personalIdentity.religion.name;

            if(personalIdentity.classYear === "" || personalIdentity.classYear === undefined){
                personalIdentity.classYear = null;
            }else{
                personalIdentity.classYear = new Date(personalIdentity.classYear,0,0);
                
                const year = personalIdentity.classYear.getFullYear();

                personalIdentity.classYear = new Date(year + 2, 0, 0);
            }
            if(personalIdentity.birthDate === "" || personalIdentity.birthDate === undefined){
                personalIdentity.birthDate = null;
            }else{
                personalIdentity.birthDate = new Date(personalIdentity.birthDate);
            }

            if (personalIdentity.phoneNumber) {
                personalIdentity.phoneNumber = personalIdentity.phoneNumber.slice(3);
            }

            const photoPreview  = personalIdentity.photo;
            delete personalIdentity['photo'];
            this.setState({
                personalIdentity,
                photoPreview
            });
        })
        .catch(error => {
            console.log(error.message);
        })
    }

    componentDidMount(){
        if(this.state.religions.length === 0 && !this.state.isLoaded){
            this.getReligions();
        }
        if(this.state.userId && !this.state.isLoaded){
            this.getPersonalIdentityData();
        }
        this.setState({
            isLoaded: true
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {
            this.setState({
                userId: this.props.id
            }, this.getPersonalIdentityData())
        }
    }
    render(){
        const {props} = this;
        const {
            religions,
            errors,
            photoPreview,
            isLoaded,
        } = this.state;

        if(isLoaded){

            const {
                nim = "-", 
                email = "-", 
                name, 
                birthDate = null, 
                studyProgram, 
                gender, 
                religion,
                phoneNumber,
                classYear= null, 
            } = this.state.personalIdentity;
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={props.handleClose}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Button className="button-circle mr-3" variant="light" onClick={props.handleBack}>
                                <FontAwesomeIcon icon={faArrowLeft} size="lg"/>
                            </Button>
                            <Modal.Title>Informasi Detail</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                        <Form>
                            <Row className="mb-3">
                                <Col></Col>
                                <Col>
                                    <ReactRoundedImage image={photoPreview? photoPreview : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="250" imageHeight="250" roundedSize="0" className="w-100" alt="Me"/>
                                    <Form.Group className="my-4">
                                        <Form.Control name="photo" type="file" placeholder="Nama Lengkap" onChange={this.handleChange} size="sm"/>
                                    </Form.Group>
                                </Col>
                                <Col></Col>
                            </Row>
                            <Form.Group className="mb-3">
                                <Form.Label>NIM</Form.Label>
                                <Form.Control value={nim} name="nim" type="text" readOnly plaintext/>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Email</Form.Label>
                                <Form.Control value={email} name="email" type="text" readOnly plaintext/>
                            </Form.Group>
                            
                            <Form.Group className="mb-3">
                                <Form.Label>Nama Lengkap *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Control value={name} name="name" type="text" placeholder="Nama Lengkap" onChange={this.handleChange} isInvalid={errors? errors.name : false}/>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors.name : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Jenis Kelamin *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Select aria-label="-" name="gender" onChange={this.handleChange} value={gender} isInvalid={errors? errors.gender : false}>
                                        <option value={0}>-</option>
                                        <option value={1}>Laki-laki</option>
                                        <option value={2}>Perempuan</option>
                                    </Form.Select>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors.gender : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Agama *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Select aria-label="-" name="religion" onChange={this.handleChange} value={religion? religion : null} isInvalid={errors? errors.religion : false}>
                                        <option>-</option>
                                        { religions && religions.map((item) => {
                                            return (
                                                <option key={item.name} value={item.name}>{item.name}</option>
                                            )
                                        })}
                                    </Form.Select>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors.religion : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Row>
                                <Col sm={12}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Tanggal Lahir *</Form.Label>
                                        <Row>
                                            <Col>
                                                <DatePicker
                                                    selected={birthDate instanceof Date ? birthDate : null}
                                                    name="birthDate"
                                                    onChange={this.handleChangeBirthDate}
                                                    dateFormat="dd MMM yyyy"
                                                    placeholderText="Tanggal Lahir"
                                                    showDatePicker
                                                />
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Col>
                                
                            </Row>
                            <Form.Group className="mb-3">
                                <Form.Label>Telepon *</Form.Label>
                                <InputGroup hasValidation>
                                    <InputGroup.Text id="inputGroupPrepend">+62</InputGroup.Text>
                                    <Form.Control
                                    type="text"
                                    placeholder=""
                                    aria-describedby="inputGroupPrepend"
                                    name="phoneNumber"
                                    value={phoneNumber}
                                    onChange={this.handleChange}
                                    isInvalid={ errors ? !!errors.phoneNumber : false}
                                    />
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors.phoneNumber : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
    
                            <Form.Group className="mb-3">
                                <Form.Label>Program Studi *</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Select aria-label="-" name="studyProgram" onChange={this.handleChange} value={studyProgram} isInvalid={errors ? errors.studyProgram : false}>
                                        <option>-</option>
                                        <option value={1}>D3-Teknik Informatika</option>
                                        <option value={2}>D4-Teknik Informatika</option>
                                    </Form.Select>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors ? errors.studyProgram : ""}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
    
    
                            <Row>
                                <Col sm={12}>
                                    <Form.Group className="mb-3">
                                        <Form.Label>Tahun Angkatan *</Form.Label>
                                        <Row>
                                            <Col>
                                                <DatePicker
                                                    selected={classYear instanceof Date ? classYear : null}
                                                    name="classYear"
                                                    onChange={this.handleChangeClassYear}
                                                    dateFormat="yyyy"
                                                    placeholderText="Tahun"
                                                    showYearPicker
                                                />
                                            </Col>
                                        </Row>
                                    </Form.Group>
                                </Col>
                                
                            </Row>
    
                        </Form>
                        </Modal.Body>
    
                        <Modal.Footer>
                            <div className="ml-auto">
                                <Button className="button-pill" variant="primary" onClick={this.handleSave}>Simpan</Button>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </>
            );
        }else{
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={props.handleClose}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Button className="button-circle mr-3" variant="light" onClick={props.handleBack}>
                                <FontAwesomeIcon icon={faArrowLeft} size="lg"/>
                            </Button>
                            <Modal.Title>Informasi Detail</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                            <h3>Loading...</h3>
                        </Modal.Body>
                    </Modal>
                </>
            )
        }
    }
}

export default EditPersonalIdentityModal;