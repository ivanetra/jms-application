import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
    InputGroup
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import {API_URL} from "../constants";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import ReactRoundedImage from "react-rounded-image";
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";

class AddPersonalIdentity extends Component{
    constructor(props) {
        super(props);
        this.state = {
            userId: "",
            userData: props.location.state? props.location.state.userData : null,
            religions: [],
            personalIdentity: {},
            isLoaded: false,
            errors: {}
        };
        this.getPersonalIdentityData = this.getPersonalIdentityData.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeClassYear = this.handleChangeClassYear.bind(this);
        this.handleChangeBirthDate = this.handleChangeBirthDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            if (this.state.errors.name) {
                if (name === "name") {
                    delete this.state.errors['name'];
                }
            }
            if (this.state.errors.gender) {
                if (name === "gender") {
                    delete this.state.errors['gender'];
                }
            }
            if (this.state.errors.religion) {
                if (name === "religion") {
                    delete this.state.errors['religion'];
                }
            }
            if (this.state.errors.phoneNumber) {
                if (name === "phoneNumber") {
                    delete this.state.errors['phoneNumber'];
                }
            }
            if (this.state.errors.studyProgram) {
                if (name === "studyProgram") {
                    delete this.state.errors['studyProgram'];
                }
            }
        }

        if(name === "photo"){
            let file = e.target.files.item(0);
            let reader = new FileReader();
            let url = reader.readAsDataURL(file);

            reader.onloadend = function (e) {
                this.setState((prevState) => ({
                    photoPreview: [reader.result]
                }))
            }.bind(this);

            this.setState(prevState => ({
                personalIdentity:{
                    ...prevState.personalIdentity, photo: file
                }
            }), () => console.log(this.state.personalIdentity));
        }else{
            if (name === "phoneNumber") {
                if ((/^\d+$/.test(value) || value === "") && value.length <= 11) {
                    this.setState(prevState => ({
                        personalIdentity:{
                            ...prevState.personalIdentity, phoneNumber: value
                        }
                    }));
                }
            } else {
                this.setState(prevState => ({
                    personalIdentity:{
                        ...prevState.personalIdentity, [name]: value
                    }
                }));
            }
        }
    }
    
    handleChangeClassYear(classYear){
        const { errors } = this.state;
        
        if (errors) {
            if (errors.classYear) {
                delete errors['classYear'];
            }
        }

        this.setState(prevState => ({
            personalIdentity:{
                ...prevState.personalIdentity, classYear
            }
        }));
    }

    handleChangeBirthDate(birthDate){
        const { errors } = this.state;
        
        if (errors) {
            if (errors.birthDate) {
                delete errors['birthDate'];
            }
        }

        this.setState(prevState => ({
            personalIdentity:{
                ...prevState.personalIdentity, birthDate
            }
        }));
    }
    
    handleSave(){
        const id = this.props.getNim();
        const { personalIdentity } = this.state;
        // personalIdentity.studyProgram = Number(personalIdentity.studyProgram);

        if (personalIdentity.gender === "0") {
            delete personalIdentity['gender'];
        }

        if (personalIdentity.religion === "-") {
            delete personalIdentity['religion'];
        }
        
        if (!personalIdentity.birthDate) {
            personalIdentity.birthDate = "";
        }

        if (personalIdentity.classYear) {
            personalIdentity.classYear = personalIdentity.classYear.getFullYear();
        }

        if (personalIdentity.phoneNumber) {
            personalIdentity.phoneNumber = "+62" + personalIdentity.phoneNumber;
        }

        if (personalIdentity.studyProgram === "-") {
            personalIdentity.studyProgram = ""
        }
        
        if(this.state.photoPreview === undefined){
            delete personalIdentity['photo'];
        } else {
            // personalIdentity.photo = this.state.photoPreview[0];
        }

        const token = this.props.getToken();

        let fd = new FormData();
        Object.keys(personalIdentity).forEach((key) => {
            fd.append(key, personalIdentity[key]);
        })

        axios.put(API_URL + `users/${id}`, fd, {
            headers: {
                contentType: "multipart/form-data",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            const { userData } = this.state;

            sessionStorage.clear()

            axios.post("https://api.jobmatcher.me/v1/auth/login", userData, {
                headers: {
                    accept: "application/json",
                    contentType: "application/json"
                }
            })
            .then(response => {
                console.log(response);

                const result = response.data.result;

                if (result.errors) {
                    this.setState({
                        errors: result.errors
                    })
                } else {
                    this.props.setUser(result);

                    this.setState({
                        formData: {}
                    });

                    this.props.history.push({pathname: '/jobs'});
                }
            })
            .catch(error => {
                console.log(error.response);
            });
        })
        .catch((error) => {
            console.log(error)

            if (error.response) {
                const errors = error.response.data.errors;

                this.setState({
                    errors: errors
                })

                if (this.state.personalIdentity.birthDate) {
                    this.setState(prevState => ({
                        personalIdentity: {
                            ...prevState.personalIdentity,
                            birthDate: new Date(this.state.personalIdentity.birthDate)
                        }
                    }))
                }

                if (this.state.personalIdentity.classYear) {
                    this.setState(prevState => ({
                        personalIdentity: {
                            ...prevState.personalIdentity,
                            classYear: new Date(this.state.personalIdentity.classYear + 1, 0, 0)
                        }
                    }))
                }

                if (this.state.personalIdentity.phoneNumber) {
                    this.setState(prevState => ({
                        personalIdentity: {
                            ...prevState.personalIdentity,
                            phoneNumber: this.state.personalIdentity.phoneNumber.slice(3)
                        }
                    }))
                }
            }
        })
    }

    getReligions(){
        const token = this.props.getToken();

        axios.get(API_URL + 'religions', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                religions: response.data.result
            });
        })
        .catch(error => {
            console.log(error);
        })
    }

    getPersonalIdentityData(){
        const token = this.props.getToken();
        const id = this.props.getNim();

        axios.get(API_URL + `users/${id}`, {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response);
            
            const personalIdentity = response.data.user;

            personalIdentity.studyProgram = "";
            personalIdentity.gender = "";
            personalIdentity.religion = "";

            if(personalIdentity.classYear === "" || personalIdentity.classYear === undefined){
                personalIdentity.classYear = "";
            }else{
                personalIdentity.classYear = new Date(personalIdentity.classYear,0,0);
                
                const year = personalIdentity.classYear.getFullYear();

                personalIdentity.classYear = new Date(year + 2, 0, 0);
            }
            if(personalIdentity.birthDate === "" || personalIdentity.birthDate === undefined){
                personalIdentity.birthDate = "";
            }else{
                personalIdentity.birthDate = new Date(personalIdentity.birthDate);
            }

            if (personalIdentity.phoneNumber) {
                personalIdentity.phoneNumber = personalIdentity.phoneNumber.slice(3);
            }

            const photoPreview  = personalIdentity.photo;
            delete personalIdentity['photo'];
            this.setState({
                personalIdentity,
                photoPreview
            });
        })
        .catch(error => {
            console.log(error);
        })
    }

    componentDidMount(){
        if (!this.props.location.state) {
            this.props.history.push('/login');
        } else {
            if(this.state.religions.length === 0 && !this.state.isLoaded){
                this.getReligions();
            }
    
            if(this.state.personalIdentity && !this.state.isLoaded) {
                this.getPersonalIdentityData();
            }
    
            this.setState({
                isLoaded: true
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {
            this.setState({
                userId: this.props.id
            }, this.getPersonalIdentityData())
        }
    }
    render(){
        const {props} = this;

        if (!props.location.state) {
            this.props.history.push('/login');
        }

        const {
            religions,
            errors,
            photoPreview,
            isLoaded,
        } = this.state;

        if(isLoaded){

            const {
                nim = "-", 
                email = "-", 
                name, 
                birthDate = null, 
                studyProgram, 
                gender, 
                religion,
                phoneNumber,
                classYear= null, 
            } = this.state.personalIdentity;
            return (
                <div className="bg-2 flex-grow-1 pb-5">
                    <div className="container-fluid">
                        <div className="grid row pt-3 root-padding">
                            <div className="card w-100 p-4">
                                    <Form>
                                        <div className="d-flex justify-content-center">
                                            <Row className="mb-3">
                                                <Col></Col>
                                                <Col>
                                                    <ReactRoundedImage image={photoPreview? photoPreview : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="152" imageHeight="152" roundedSize="0" className="w-100" alt="Me"/>
                                                    <Form.Group className="my-4">
                                                        <Form.Control name="photo" type="file" placeholder="Nama Lengkap" onChange={this.handleChange} size="sm"/>
                                                    </Form.Group>
                                                </Col>
                                                <Col></Col>
                                            </Row>
                                        </div>
                                            <Form.Group className="mb-3">
                                                <Form.Label>Nama Lengkap *</Form.Label>
                                                <InputGroup hasValidation>
                                                    <Form.Control value={name} name="name" type="text" placeholder="Nama Lengkap" onChange={this.handleChange} isInvalid={errors? errors.name : false}/>
                                                    <Form.Control.Feedback type="invalid" tooltip>
                                                        {errors ? errors.name : ""}
                                                    </Form.Control.Feedback>
                                                </InputGroup>
                                            </Form.Group>
                                            <Form.Group className="mb-3">
                                                <Form.Label>Tanggal Lahir *</Form.Label>
                                                <DatePicker
                                                    selected={birthDate instanceof Date ? birthDate : null}
                                                    name="birthDate"
                                                    onChange={this.handleChangeBirthDate}
                                                    dateFormat="dd MMM yyyy"
                                                    placeholderText="Tanggal Lahir"
                                                    showDatePicker
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    dropdownMode="select"
                                                />
                                                {typeof errors.birthDate === "undefined"?
                                                    null 
                                                :
                                                    <div className="mt-2 text-danger font-size-14">
                                                        {errors.birthDate}
                                                    </div>
                                                }
                                            </Form.Group>
                                            <Form.Group className="mb-3">
                                                <Form.Label>Jenis Kelamin *</Form.Label>
                                                <InputGroup hasValidation>
                                                    <Form.Select aria-label="-" name="gender" onChange={this.handleChange} value={gender} isInvalid={errors? errors.gender : false}>
                                                        <option value={0}>-</option>
                                                        <option value={1}>Laki-laki</option>
                                                        <option value={2}>Perempuan</option>
                                                    </Form.Select>
                                                    <Form.Control.Feedback type="invalid" tooltip>
                                                        {errors ? errors.gender : ""}
                                                    </Form.Control.Feedback>
                                                </InputGroup>
                                            </Form.Group>
                                            <Form.Group className="mb-3">
                                                <Form.Label>Agama *</Form.Label>
                                                <InputGroup hasValidation>
                                                    <Form.Select aria-label="-" name="religion" onChange={this.handleChange} value={religion? religion : null} isInvalid={errors? errors.religion : false}>
                                                        <option>-</option>
                                                        { religions && religions.map((item) => {
                                                            return (
                                                                <option key={item.name} value={item.name}>{item.name}</option>
                                                            )
                                                        })}
                                                    </Form.Select>
                                                    <Form.Control.Feedback type="invalid" tooltip>
                                                        {errors ? errors.religion : ""}
                                                    </Form.Control.Feedback>
                                                </InputGroup>
                                            </Form.Group>
                                            <Form.Group className="mb-3">
                                                <Form.Label>Program Studi *</Form.Label>
                                                <InputGroup hasValidation>
                                                    <Form.Select aria-label="-" name="studyProgram" onChange={this.handleChange} value={studyProgram} isInvalid={errors ? errors.studyProgram : false}>
                                                        <option>-</option>
                                                        <option value={1}>D3-Teknik Informatika</option>
                                                        <option value={2}>D4-Teknik Informatika</option>
                                                    </Form.Select>
                                                    <Form.Control.Feedback type="invalid" tooltip>
                                                        {errors ? errors.studyProgram : ""}
                                                    </Form.Control.Feedback>
                                                </InputGroup>
                                            </Form.Group>
                        
                        
                                            <Row>
                                                <Col sm={12}>
                                                    <Form.Group className="mb-3">
                                                        <Form.Label>Tahun Angkatan *</Form.Label>
                                                        <Row>
                                                            <Col>
                                                                <DatePicker
                                                                    selected={classYear instanceof Date ? classYear : null}
                                                                    name="classYear"
                                                                    onChange={this.handleChangeClassYear}
                                                                    dateFormat="yyyy"
                                                                    placeholderText="Tahun"
                                                                    showYearPicker
                                                                />
                                                                {typeof errors.classYear === "undefined"?
                                                                    null 
                                                                :
                                                                    <div className="mt-2 text-danger font-size-14">
                                                                        {errors.classYear}
                                                                    </div>
                                                                }
                                                            </Col>
                                                        </Row>
                                                    </Form.Group>
                                                </Col>
                                            </Row>
                                            <Form.Group className="mb-3">
                                                <Form.Label>Telepon *</Form.Label>
                                                <InputGroup hasValidation>
                                                    <InputGroup.Text id="inputGroupPrepend">+62</InputGroup.Text>
                                                    <Form.Control
                                                        type="text"
                                                        placeholder=""
                                                        aria-describedby="inputGroupPrepend"
                                                        name="phoneNumber"
                                                        value={phoneNumber}
                                                        onChange={this.handleChange}
                                                        isInvalid={ errors ? !!errors.phoneNumber : false}
                                                    />
                                                    <Form.Control.Feedback type="invalid" tooltip>
                                                        {errors ? errors.phoneNumber : ""}
                                                    </Form.Control.Feedback>
                                                </InputGroup>
                                            </Form.Group>
                                            <div className="d-flex">
                                                <div className="ml-auto">
                                                    <Button className="button-pill bg-3" onClick={this.handleSave}>Simpan</Button>
                                                </div>
                                            </div>
                                        </Form>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }else{
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={props.handleClose}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Button className="button-circle mr-3" variant="light" onClick={props.handleBack}>
                                <FontAwesomeIcon icon={faArrowLeft} size="lg"/>
                            </Button>
                            <Modal.Title>Informasi Detail</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                            <h3>Loading...</h3>
                        </Modal.Body>
                    </Modal>
                </>
            )
        }
    }
}

AddPersonalIdentity.propTypes = {
    getToken: PropTypes.func.isRequired,
    getNim: PropTypes.func.isRequired,
    setUser: PropTypes.func.isRequired
}

export default withRouter(AddPersonalIdentity);