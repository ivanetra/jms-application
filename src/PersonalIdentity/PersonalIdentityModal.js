import { Component } from "react";
import { 
    Modal,
    Button,
    Row,
    Col
} from "react-bootstrap";
import {API_URL} from "../constants";
import axios from "axios";
import { 
    faBirthdayCake,
    faCalendarAlt,
    faEnvelope,
    faPhoneAlt, 
    faPrayingHands, 
    faUserGraduate, 
    faVenusMars, 
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EditPersonalIdentityModal from "./EditPersonalIdentityModal";
import moment from 'moment';

class PersonalIdentityModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            userId: props.id,
            edit: false,
            personalIdentity: {

            }
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;
        this.setState(prevState => ({
            personalIdentity:{
                ...prevState.personalIdentity, [name]: value
            }
        }));
    }
    
    getPersonalIdentityData(){
        const token = this.props.getToken();
        const id = this.state.userId;
        axios.get(API_URL + `users/${id}`, {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            const personalIdentity = response.data.user;

            if(personalIdentity.birthDate === "" || personalIdentity.birthDate === undefined){
                personalIdentity.birthDate = null;
            }else{
                personalIdentity.birthDate = moment(new Date(personalIdentity.birthDate)).format("DD MMMM yyyy");
            }

            this.setState({
                personalIdentity
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
    componentDidMount(){
        if(this.state.userId && this.props.show){
            this.getPersonalIdentityData();
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {
            this.setState({
                userId: this.props.id
            }, this.getPersonalIdentityData())
        }
    }

    render(){
        const {
            name,
            studyProgram,
            classYear,
            phoneNumber,
            birthDate,
            gender,
            religion,
            email
        } = this.state.personalIdentity;

        const {props} = this;
        const {edit} = this.state;
        if(!edit){

            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={props.handleClose}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>{name}</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                        <Row>
                            <Col>
                                <Modal.Title className="font-weight-bold">Informasi Detail</Modal.Title>
                            </Col>
                            <Col xs={2} lg={1}>
                                <Button className="button-circle ml-auto" variant="light" onClick={() => this.setState({edit: true})}>
                                    <img className="edit-action ml-auto" src="/img/Pencil.svg" alt="Edit Action" width="18" height="18"></img>
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faBirthdayCake} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Tanggal Lahir</label>
                                <p>{birthDate ? birthDate : '-'}</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faVenusMars} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Jenis Kelamin</label>
                                <p>{gender ? gender.name : "-"}</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faPrayingHands} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Agama</label>
                                <p>{religion ? religion.name : "-"}</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faEnvelope} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Email</label>
                                <p>{email ? email : '-'}</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faPhoneAlt} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Telepon</label>
                                <p>{phoneNumber ? phoneNumber : '-'}</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faUserGraduate} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Program Studi</label>
                                <p>{studyProgram ? studyProgram.name : "-"}</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="mx-auto px-4" xs={2} lg={1}>
                                <FontAwesomeIcon className="fa-2x" icon={faCalendarAlt} size="lg"/>
                            </Col>
                            <Col>
                                <label className="font-weight-bold">Tahun Angkatan</label>
                                <p>{classYear ? classYear : '-'}</p>
                            </Col>
                        </Row>
                        </Modal.Body>
                    </Modal>
                </>
            );
        }else{
            return (
                <EditPersonalIdentityModal
                    id={props.id}
                    show={props.show}
                    handleClose={(message) => {this.setState({edit: false}); props.handleClose(message);}} 
                    handleBack={() => this.setState({edit: false})} 
                    getToken={this.props.getToken}
                />
            )
        }
    }
}

export default PersonalIdentityModal;