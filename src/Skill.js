import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";

 
class Skill extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: true
        };
    }

    componentDidMount() {
        
    }

    render() {
        const { isLoaded } = this.state;
        if (isLoaded) {
            return (
                <div className="bg-2 flex-grow-1">
                    <div className="container-fluid">
                        <div className="grid row d-flex">
                            <aside id="aside" className="grid d-flex shadow">
                                <div className="aside-brand d-flex w-100 bg-3 px-4-2 py-2-5">
                                    <NavLink to="/skill" className="navbar-brand brand mr-0 font-size-58 text-white">
                                        <b>JMS</b>
                                    </NavLink>
                                </div>
                                <div className="aside-menu-wrapper bg-white pt-4-2-5">
                                    <ul className="menu-nav pl-0">
                                        <li className="menu-item d-flex">
                                            <NavLink to="/skill" className="menu-link d-flex w-100 bg-4">
                                                <span className="menu-link-icon d-flex">
                                                    <img className="skill-icon" src="/img/Skill.svg" alt="Skill" width="24" height="24"></img>
                                                </span>
                                                <span className="menu-link-text d-flex ml-3 text-6 font-size-13">Keahlian</span>
                                            </NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <div></div>
        }
        
    }
}
 
export default withRouter(Skill);