import { Component } from "react";
import {Toast} from "react-bootstrap";

class NotificationToast extends Component{
    constructor(props){
        super(props);
        this.state = {
            body: props.body,
            header: props.header ? props.header : "",
            show: props.show
        };
    }
    render(){
        const {
            header,
            body,
            show
        } = this.state;
        return (
            <Toast onClose={this.props.onClose} show={show} delay={5000} autohide>
            <Toast.Header>
                <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
                <strong className="me-auto">{header}</strong>
            </Toast.Header>
            <Toast.Body>{body}</Toast.Body>
            </Toast>
        )
    }
}

export default NotificationToast;