import React from "react";
import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom";
import Main from "./Main";
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-datepicker/dist/react-datepicker.css";
import "./css/index.css";
import "./css/custom.css";

ReactDOM.render( 
    <BrowserRouter>
        <Main />
    </BrowserRouter>
    ,
    document.getElementById("root")
);