import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import axios from "axios";
import { Form, InputGroup } from "react-bootstrap";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        email: "",
        password: "",
      },
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e){
    let value = e.target.value;
    let name = e.target.name;

    if (this.state.errors) {
      if (this.state.errors.email) {
          if (name === "email") {
              delete this.state.errors['email'];
          }
      }

      if (this.state.errors.password) {
          if (name === "password") {
              delete this.state.errors['password'];
          }
      }
    }

    this.setState(prevState => ({
        formData:{
            ...prevState.formData, [name]: value
        }
    }));
}

  handleSubmit(event) {
    event.preventDefault();

    const { formData } = this.state;

    console.log(formData)

    axios.post("https://api.jobmatcher.me/v1/auth/login", formData, {
      headers: {
        accept: "application/json"
      }
    })
    .then(response => {
      console.log(response);
      const result = response.data.result;
      console.log(result)
      if (result.errors) {
        this.setState({
            errors: result.errors
        })
      } else {
          this.props.setUser(result);

          this.setState({
              formData: {}
          });

          this.props.history.push('/jobs');
      }
    })
    .catch(error => {
      console.log(error.response);
      const errors = error.response.data.errors;
      const message = error.response.data.message;

      if (errors) {
        this.setState({errors: errors});
      }

      if(message) {
        if (message === "User tidak ditemukan") {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              email: message
            }
          }))
        } else if (message === "Email user belum terverifikasi") {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              email: message
            }
          }))
        } else {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              password: message
            }
          }))
        }
      }
    });
  }

  render() {
    const { errors } = this.state;
    const { email, password } = this.state.formData;

    return (
        <div>
            <div className="container-fluid">
                <section className="section-login">
                  <div className="text-center">
                    <NavLink to="/" className="jms-logo text-center text-1"><b>JMS</b></NavLink>
                  </div>
                  <div className="card mt-4">
                    <form onSubmit={this.handleSubmit}>
                      <h1 className="font-size-32">Login</h1>
                      <InputGroup hasValidation>
                        <Form.Control className="mt-4" value={email} name="email" type="email" placeholder="Email" onChange={this.handleChange} isInvalid={errors? (typeof errors.email === "undefined"? false : errors.email) : false} required autoFocus/>
                        <Form.Control.Feedback type="invalid" tooltip>
                          {errors? (typeof errors.email === "undefined"? "" : errors.email) : ""}
                        </Form.Control.Feedback>
                      </InputGroup>
                      <InputGroup hasValidation>
                        <Form.Control className="mt-4" value={password} name="password" type="password" placeholder="Kata Sandi" onChange={this.handleChange} isInvalid={errors? (typeof errors.password === "undefined"? false : errors.password) : false} required/>
                        <Form.Control.Feedback type="invalid" tooltip>
                          {errors? (typeof errors.password === "undefined"? "" : errors.password) : ""}
                        </Form.Control.Feedback>
                      </InputGroup>
                      <button className="login-action mt-3 w-100" type="submit">Login</button>
                    </form>
                  </div>
                  <div className="join-now text-center">
                    Baru bergabung di JMS?
                    <NavLink to="/signup" id="join-now" className="ml-2 text-1">Bergabung sekarang</NavLink>
                  </div>           
                </section>
            </div>
        </div>
    );
  }
}

Login.propTypes = {
  setUser: PropTypes.func.isRequired
}
 
export default withRouter(Login);