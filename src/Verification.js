import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import axios from "axios";
import { Form, InputGroup } from "react-bootstrap";

class Verification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        email: props.location.state? props.location.state.email : null,
        code: ""
      },
      userData: {
        email: props.location.state? props.location.state.email : null,
        password: props.location.state? props.location.state.password: null
      },
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e){
    let value = e.target.value;

    if (this.state.errors) {
        if (this.state.errors.code) {
            delete this.state.errors['code'];
        }
    }

    if (/^\d+$/.test(value) || value === "") {
        if (value.length <= 6) {
            this.setState(prevState => ({
                formData:{
                    ...prevState.formData,
                    code: value
                }
            }));
        }
    }
}

  handleSubmit(event) {
    event.preventDefault();

    const { formData, userData } = this.state;

    axios.post("https://api.jobmatcher.me/v1/auth/validateEmail", formData, {
      headers: {
        accept: "application/json",
        contentType: "application/json"
      }
    })
    .then(response => {
      const result = response.data.result;
      
      if (result.errors) {
        this.setState({
            errors: result.errors
        })
      } else {
        axios.post("https://api.jobmatcher.me/v1/auth/login", userData, {
          headers: {
            accept: "application/json",
            contentType: "application/json"
          }
        })
        .then(response => {
          console.log(response);

          const result = response.data.result;

          if (result.errors) {
            this.setState({
                errors: result.errors
            })
          } else {
              this.props.setUser(result);

              this.setState({
                  formData: {}
              });

              this.props.history.push({pathname: '/new-profile', state: {userData: userData}});
          }
        })
        .catch(error => {
          console.log(error.response);
        });
      }
    })
    .catch(error => {
        console.log(error.response);

        const message = error.response.data.message;

        this.setState(prevState => ({
            errors:{
                ...prevState.formData,
                code: message
            }
        }));
    });
  }

  render() {
    if (!this.props.location.state) {
      console.log(this.props.location.state)
      this.props.history.push('/login');
    }

    const { errors } = this.state;
    const { code } = this.state.formData;

    return (
        <div className="flex-grow-1">
            <div className="container-fluid">
                <section className="section-verification text-center">
                    <h4 className="mb-0">Konfirmasikan email anda</h4>
                    <p className="mt-2-5 mb-0">Masukkan kode yang telah kami kirimkan ke email Anda</p>
                    <Form className="mt-5" onSubmit={this.handleSubmit}>
                        <Form.Group className="d-flex">
                            <Form.Control className="verification-form mx-auto text-center" value={code} type="text" onChange={this.handleChange} isInvalid={errors? (typeof errors.code === "undefined"? false : errors.code) : false} required autoFocus/>
                            {typeof errors.code === "undefined"?
                                null 
                            :
                                <div className="mt-2 text-center text-danger font-size-14">
                                    {errors.code}
                                </div>
                            }
                        </Form.Group>
                        <button className="verification-form verify-action mt-4 w-100" type="submit">Konfirmasi</button>
                    </Form>
                </section>
            </div>
        </div>
    );
  }
}

Verification.propTypes = {
  setUser: PropTypes.func.isRequired
}
 
export default withRouter(Verification);