import { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col,
} from "react-bootstrap";
import {API_URL} from "../constants";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

class ApplyJobModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            jobId: props.id,
            jobApplication: {
            },
            isLoaded: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            
            if (this.state.errors.applicantDocuments) {
                if (name === "applicantDocuments") {
                    delete this.state.errors['applicantDocuments'];
                }
            }
        }

        if(name === "applicantDocuments"){
            let file = e.target.files.item(0);

            this.setState(prevState => ({
                jobApplication:{
                    ...prevState.jobApplication, applicantDocuments: file
                }
            }));
        }else{
            this.setState(prevState => ({
                jobApplication:{
                    ...prevState.jobApplication, [name]: value
                }
            }));
        }
    }
    
    
    
    handleSave(){
        const {jobApplication, jobId} = this.state;       
        
        const token = this.props.getToken();

        let fd = new FormData();
        Object.keys(jobApplication).forEach((key) => {
            fd.append(key, jobApplication[key]);
        })


        axios.post(API_URL + `jobs/${jobId}/apply`, fd, {
            headers: {
              contentType: "multipart/form-data",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            if (!response.data.errors) {
                this.setState({
                    jobApplication: {}
                });
            }
            
            console.log(response.data);
            if(response.data.status){
                this.props.handleClose("Berhasil!" ,"Lamaran pekerjaan berhasil dikirimkan.");
            }else{
                this.props.handleClose("Gagal!", response.data.message);

            }
            
        })
        .catch((error) => {
            let data = {
                message: ""
            }
            if(error.response){
                data = error.response.data;

            }
            
            // this.setState({
            //     errors
            // })
            console.log(data)
            this.props.handleClose("Gagal!", data.message);
            
        })
    }


   

    componentDidMount(){
        this.setState({
            isLoaded: true
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.show !== prevProps.show) {

        }
    }
    render(){
        const {props} = this;
        const {
            isLoaded,
        } = this.state;
        if(isLoaded){
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={props.handleClose}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Melamar</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                        <Form>
                            <Row className="mb-3">
                                <Form.Label className="font-weight-bold">Syarat Dokumen</Form.Label>
                                <p>{this.props.documentRequirement ? this.props.documentRequirement : "Tidak ada syarat dokumen. Silahkan lewati pengunggahan dokumen dan klik tombol Lamar."}</p>
                                <Col>
                                    <Form.Group className="my-4">
                                        <Form.Label className="font-weight-bold">Unggah Dokumen</Form.Label>
                                        <Form.Control name="applicantDocuments" type="file" placeholder="Dokumen" onChange={this.handleChange} size="md"/>
                                        <Form.Text className="text-muted">
                                            Format: .zip
                                        </Form.Text>
                                    </Form.Group>
                                </Col>
                            </Row>
    
                        </Form>
                        </Modal.Body>
    
                        <Modal.Footer>
                            <div className="ml-auto">
                                <Button className="button-pill" variant="primary" onClick={this.handleSave}>Lamar</Button>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </>
            );
        }else{
            return (
                <>                
                    <Modal
                        show={props.show}
                        onHide={props.handleClose}
                        size="lg"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Button className="button-circle mr-3" variant="light" onClick={props.handleBack}>
                                <FontAwesomeIcon icon={faArrowLeft} size="lg"/>
                            </Button>
                            <Modal.Title>Melamar</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body>
                            <h3>Loading...</h3>
                        </Modal.Body>
                    </Modal>
                </>
            )
        }
    }
}

export default ApplyJobModal;