import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from 'axios';
import PropTypes from 'prop-types';
import {API_URL} from "../constants";
import { Row, Col, Form, InputGroup, Button } from "react-bootstrap";
import { faBuilding } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
 
class PostJob extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            formData: {
                companyLogo: "",
                title: "",
                companyName: "",
                location: "",
                remote: false,
                quantity: 1,
                jobType: 0,
                description: "",
                benefits: "",
                minSalary: "",
                maxSalary: "",
                endDate: "",
                contact: "",
                requirements: {
                    requiredSkills: [],
                    softSkillRequirement: "",
                    maximumAge: 0,
                    classYearRequirement: [],
                    requiredGender: [],
                    requiredReligion: [],
                    studyProgramRequirement: [],
                    documentRequirement: "",
                    description: ""
                }
            },
            photoPreview: "",
            classYearOptions: [],
            gender: [false, false],
            religion:[false, false, false, false, false, false],
            studyProgram: [false, false],
            isLoaded: false,
            requiredSkillInputValue: "",
            classYearRequirementInputValue: "",
            requiredSkills: [],
            classYearRequirement: [],
            notificationToast: [],
            errors: {}
        };
        this.getClassYearOptionData = this.getClassYearOptionData.bind(this);
        this.handleInputSkillChange = this.handleInputSkillChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeSkills = this.handleChangeSkills.bind(this);
        this.handleChangeClassYear = this.handleChangeClassYear.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.loadSkillOptions = this.loadSkillOptions.bind(this);
    }

    componentDidMount() {
        this.getClassYearOptionData();
    }

    getClassYearOptionData() {
        const currentYear = new Date().getFullYear();

        let classYearOptions = [];

        for(let classYear = 1997; classYear <= currentYear; classYear++) {
            classYearOptions.push({
                label: classYear,
                value: classYear
            })
        }

        this.setState({
            classYearOptions: classYearOptions,
            isLoaded: true
        });
    }

    handleInputSkillChange(skillInputValue){
        this.setState({
            requiredSkillInputValue: skillInputValue
        });
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;

        if (this.state.errors) {
            if (this.state.errors.title) {
                if (name === "title") {
                    delete this.state.errors['title'];
                }
            }

            if (this.state.errors.jobType) {
                if (name === "jobType") {
                    delete this.state.errors['jobType'];
                }
            }
      
            if (this.state.errors.quantity) {
                if (name === "quantity") {
                    delete this.state.errors['quantity'];
                }
            }

            if (this.state.errors.contact) {
                if (name === "contact") {
                    delete this.state.errors['contact'];
                }
            }
        }

        if (name === "jobType") {
            if (value === "1") {
                value = Number(value);
            } else {
                value = Number(value);
            }
        }

        if(name === "companyLogo"){
            let file = e.target.files.item(0);
            let reader = new FileReader();
            let url = reader.readAsDataURL(file);

            reader.onloadend = function (e) {
                this.setState((prevState) => ({
                    photoPreview: [reader.result]
                }))
            }.bind(this);

            this.setState(prevState => ({
                formData:{
                    ...prevState.formData, companyLogo: file
                }
            }));
        }else if (name === "quantity") {
            if (Number(value) >= 1) {
                this.setState(prevState => ({
                    formData:{
                        ...prevState.formData, [name]: value
                    }
                }));
            }
        } else if (name === "requirements") {
            let requirementName = e.currentTarget.getAttribute("requirementname");
            
            if (requirementName === "maximumAge") {
                if (/^\d+$/.test(value) || value === "") {
                    const firstYear = 1997;
                    const currentYear = new Date().getFullYear();
                    const maximumAgeConstraint = currentYear - firstYear + 17;

                    if (Number(value) <= maximumAgeConstraint) {
                        this.setState(prevState => ({
                            formData: {
                                ...prevState.formData,
                                requirements: {
                                    ...prevState.formData.requirements,
                                    maximumAge: value
                                }
                            }
                        }))
                    }
                }
            } else if(requirementName === "requiredGender") {
                const genderIndex = Number(e.currentTarget.getAttribute("indexkey"));
                const checkedValue = e.target.checked;
                
                this.setState(prevState => ({
                    gender: prevState.gender.map((gender, index) => (
                        genderIndex === index? checkedValue : gender
                    ))
                }))
            } else if(requirementName === "requiredReligion") {
                const religionIndex = Number(e.currentTarget.getAttribute("indexkey"));
                const checkedValue = e.target.checked;
                
                this.setState(prevState => ({
                    religion: prevState.religion.map((religion, index) => (
                        religionIndex === index? checkedValue : religion
                    ))
                }))
            } else if(requirementName === "studyProgramRequirement") {
                const studyProgramIndex = Number(e.currentTarget.getAttribute("indexkey"));
                const checkedValue = e.target.checked;
                
                this.setState(prevState => ({
                    studyProgram: prevState.studyProgram.map((studyProgram, index) => (
                        studyProgramIndex === index? checkedValue : studyProgram
                    ))
                }))
            } else {
                
                this.setState(prevState => ({
                    formData: {
                        ...prevState.formData,
                        requirements: {
                            ...prevState.formData.requirements,
                            [requirementName]: value
                        }
                    }
                }))
                
            }
        }else if(name === "remote") {
            const remote = e.target.checked;

            this.setState(prevState => ({
                formData: {
                    ...prevState.formData,
                    remote: remote
                }
            }))
        } else if (name === "minSalary" || name === "maxSalary"){
            if (/^\d+$/.test(value) || value === "") {
                this.setState(prevState => ({
                    formData:{
                        ...prevState.formData, [name]: value
                    }
                }));
            }
        }else {
            this.setState(prevState => ({
                formData:{
                    ...prevState.formData, [name]: value
                }
            }));
        }
    }

    handleChangeSkills(skills) {
        if (this.state.errors) {
            if (this.state.errors["requirements.requiredSkills"]) {
                delete this.state.errors["requirements.requiredSkills"];
            }
        }

        this.setState(prevState => ({
            formData: {
                ...prevState.formData,
                requirements: {
                    ...prevState.formData.requirements,
                    requiredSkills: skills
                }
            }
        }))
    }

    handleChangeClassYear(classYears) {
        this.setState(prevState => ({
            formData: {
                ...prevState.formData,
                requirements: {
                    ...prevState.formData.requirements,
                    classYearRequirement: classYears
                }
            }
        }))
    }

    handleChangeEndDate(endDate) {
        if (this.state.errors) {
            if (this.state.errors.endDate) {
                delete this.state.errors['endDate'];
            }
        }

        this.setState(prevState => ({
            formData: {
                ...prevState.formData,
                endDate: endDate 
            }
        }))
    }

    handleSave(){
        const {formData, gender, religion, studyProgram} = this.state;
        
        console.log(formData)

        if(!this.state.photoPreview){
            delete formData['companyLogo'];
        } else {
            //formData.companyLogo = this.state.photoPreview[0];
        }

        if (!formData.jobType) {
            delete formData['jobType'];
        }

        if (!formData.minSalary) {
            formData.minSalary = null;
        }

        if (!formData.maxSalary) {
            formData.maxSalary = null;
        }

        gender.map((item, index) => {
            if (item) {
                const genderIndex = index + 1;
    
                formData.requirements.requiredGender.push(genderIndex);
            }
    
            return item;
        })

        if (!formData.requirements.requiredGender.length) {
            formData.requirements.requiredGender = null;
        }

        religion.map((item, index) => {
            if (item) {
                const religionIndex = index + 1;

                 formData.requirements.requiredReligion.push(religionIndex);
            }
    
            return item;
        })

        if (!formData.requirements.requiredReligion.length) {
            delete formData.requirements["requiredReligion"];
        }

        studyProgram.map((item, index) => {
            if (item) {
                const studyProgramIndex = index + 1;

                formData.requirements.studyProgramRequirement.push(studyProgramIndex);
            }
    
            return item;
        })
    
        if (!formData.requirements.studyProgramRequirement.length) {
            formData.requirements.studyProgramRequirement = null;
        }

        if (formData.requirements.classYearRequirement) {
            formData.requirements.classYearRequirement.map(function(classYear, index) { 
                formData.requirements.classYearRequirement[index] = classYear.value;

                return classYear;
            });
        }

        if (!formData.requirements.classYearRequirement.length) {
            delete formData.requirements["classYearRequirement"];
        }

        if (!formData.requirements.documentRequirement) {
            delete formData.requirements["documentRequirement"];
        }

        if (formData.requirements.requiredSkills) {
            this.setState({requiredSkills: formData.requirements.requiredSkills});

            formData.requirements.requiredSkills.map(function(skill, index) { 
                formData.requirements.requiredSkills[index] = skill.value;

                return skill
            });
        }

        if (!formData.requirements.softSkillRequirement) {
            formData.requirements.softSkillRequirement = null;
        }

        if (!formData.requirements.description) {
            formData.requirements.description = "";
        }

        if (!formData.requirements.maximumAge) {
            formData.requirements.maximumAge = null;
        }

        const token = this.props.getToken();

        delete formData["id"];
        delete formData["userId"];

        let fd = new FormData();
        Object.keys(formData).forEach((key) => {
            if (key === "requirements") {
                fd.append(key, JSON.stringify(formData[key]));
            } else {
                fd.append(key, formData[key]);
            }
        })

        console.log(formData);

        axios.post(API_URL + `jobs`, fd, {
            headers: {
                accept: "application/json",
                contentType: "multipart/form-data",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response)
            if (response.data.result.errors) {
                this.setState({
                    errors: response.data.result.errors
                })

                if (!formData.requirements.requiredGender) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            requirements: {
                                ...prevState.formData.requirements,
                                requiredGender: []
                            }
                        }
                    }))
                }

                if (!formData.requirements.studyProgramRequirement) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            requirements: {
                                ...prevState.formData.requirements,
                                studyProgramRequirement: []
                            }
                        }
                    }))
                }

                if (!formData.requirements.requiredReligion) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            requirements: {
                                ...prevState.formData.requirements,
                                requiredReligion: []
                            }
                        }
                    }))
                }

                if (!formData.requirements.classYearRequirement) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            requirements: {
                                ...prevState.formData.requirements,
                                classYearRequirement: []
                            }
                        }
                    }))
                } else {
                    const classYearRequirements = formData.requirements.classYearRequirement.map((classYear) => ({
                        label: classYear,
                        value: classYear
                    }))

                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            requirements: {
                                ...prevState.formData.requirements,
                                classYearRequirement: classYearRequirements
                            }
                        }
                    }))
                }

                if (!formData.requirements.documentRequirement) {
                    this.setState(prevState => ({
                        formData: {
                            ...prevState.formData,
                            requirements: {
                                ...prevState.formData.requirements,
                                documentRequirement: ""
                            }
                        }
                    }))
                }
            } else {
                this.setState({
                    formData: {
                        companyLogo: "",
                        title: "",
                        companyName: "",
                        location: "",
                        remote: false,
                        quantity: 1,
                        jobType: 0,
                        description: "",
                        benefits: "",
                        minSalary: "",
                        maxSalary: "",
                        endDate: "",
                        contact: "",
                        requirements: {
                            requiredSkills: [],
                            softSkillRequirement: "",
                            maximumAge: 0,
                            classYearRequirement: [],
                            requiredGender: [],
                            requiredReligion: [],
                            studyProgramRequirement: [],
                            documentRequirement: "",
                            description: ""
                        }
                    }
                });

                this.props.history.push('/posted-jobs');
            }
        })
        .catch((error) => {
            console.log("post job error: ", error.response);
        })
    }

    async loadSkillOptions(skillInputValue){
        const { requiredSkillInputValue } = this.state;
        const token = this.props.getToken();

        if (requiredSkillInputValue) {
            const { data } = await axios.get(API_URL + 'skills/search?skillQuery=' + skillInputValue, {
                headers: {
                    accept: "application/json",
                    authorization: "Bearer " + token
                }
            })
            .catch(error => {
                console.log(error);
            })
    
            const options = data.result.map((skill) => ({
                label: skill.name,
                value: skill.id
            }))
    
            return options;
        }
    }

    render() {
        const currentYear = new Date().getFullYear();
        const { photoPreview, gender, religion, studyProgram, isLoaded, classYearOptions, errors } = this.state;
        const {
            title,
            companyName,
            location,
            quantity,
            jobType,
            duration,
            description,
            minSalary,
            maxSalary,
            benefits,
            endDate,
            remote,
            contact,
            requirements
        } = this.state.formData;
        
        if (isLoaded) {
            return (
                <div className="bg-2 flex-grow-1">
                    <div className="container-fluid">
                        <div className="grid row pt-5 root-padding">
                            <div className="card mb-5 w-100 p-4">
                                <Form>
                                    <Row className="mb-4">
                                        <Col></Col>
                                        <Col className="text-center">
                                            <img className="card-photo" src={photoPreview? photoPreview : "/img/Company Logo.svg"} alt={title} width="152" height="152"></img>
                                            <Form.Group className="mt-3">
                                                <Form.Control name="companyLogo" type="file" onChange={this.handleChange} size="sm"/>
                                            </Form.Group>
                                        </Col>
                                        <Col></Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Judul <span className="text-6">*</span></Form.Label>
                                                <Form.Control value={title} name="title" type="text" placeholder="Judul (mis: Software Engineer)" onChange={this.handleChange} isInvalid={errors? (typeof errors.title === "undefined"? false : errors.title) : false} required/>
                                                <Form.Control.Feedback type="invalid" tooltip>
                                                    {errors? (typeof errors.title === "undefined"? "" : errors.title) : ""}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label htmlFor="inlineFormInputGroup">Perusahaan</Form.Label>
                                                <InputGroup className="mb-2">
                                                    <InputGroup.Text>
                                                        <FontAwesomeIcon icon={faBuilding} size="lg"/>
                                                    </InputGroup.Text>
                                                    <Form.Control  id="inlineFormInputGroup"value={companyName} name="companyName" type="text" placeholder="Perusahaan (mis: Gojek)" onChange={this.handleChange}/>
                                                </InputGroup>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Lokasi</Form.Label>
                                                <Form.Control value={location} name="location" type="text" placeholder="Lokasi (mis: Jakarta)" onChange={this.handleChange}/>
                                                <Form.Check
                                                    label="Jarak jauh"
                                                    name="remote"
                                                    type="checkbox"
                                                    id="remote"
                                                    checked={remote}
                                                    onChange={this.handleChange}
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Jumlah orang yang dibutuhkan <span className="text-6">*</span></Form.Label>
                                                <Form.Control value={quantity} name="quantity" type="number" onChange={this.handleChange} isInvalid={errors? (typeof errors.quantity === "undefined"? false : errors.quantity) : false} required/>
                                                <Form.Control.Feedback type="invalid" tooltip>
                                                    {errors? (typeof errors.quantity === "undefined"? "" : errors.quantity) : ""}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Jenis pekerjaan <span className="text-6">*</span></Form.Label>
                                                <div className="d-flex flex-row">
                                                    <Form.Check
                                                        inline
                                                        label="Full Time"
                                                        value={1}
                                                        name="jobType"
                                                        type="Radio"
                                                        id="inline-job-type-1"
                                                        className="d-flex align-items-center"
                                                        checked={jobType === 1}
                                                        onChange={this.handleChange}
                                                    />
                                                    <Form.Check
                                                        inline
                                                        label="Project"
                                                        value={2}
                                                        name="jobType"
                                                        type="radio"
                                                        id="inline-job-type-2"
                                                        className="d-flex align-items-center"
                                                        checked={jobType === 2}
                                                        onChange={this.handleChange}
                                                    />
                                                </div>
                                                {typeof errors.jobType === "undefined"?
                                                    null 
                                                :
                                                    <div className="mt-2 text-danger font-size-14">
                                                        {errors.jobType}
                                                    </div>
                                                }
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    {jobType === 2?
                                        <Row className="mb-4">
                                            <Col>
                                                <Form.Group>
                                                    <Form.Label >Durasi proyek</Form.Label>
                                                    <Form.Control value={duration} name="duration" type="text" onChange={this.handleChange}/>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                    :
                                        null
                                    }
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Deskripsi</Form.Label>
                                                <CKEditor
                                                    editor={ ClassicEditor }
                                                    data={description}
                                                    onReady={ editor => {
                                                        // You can store the "editor" and use when it is needed.
                                                        // console.log( 'Editor is ready to use!', editor );
                                                    } }
                                                    onChange={ ( event, editor ) => {
                                                        const data = editor.getData();

                                                        this.setState(prevState => ({
                                                            formData: {
                                                                ...prevState.formData,
                                                                description: data
                                                            }
                                                        }));
                                                    } }
                                                    onBlur={ ( event, editor ) => {
                                                        // console.log( 'Blur.', editor );
                                                    } }
                                                    onFocus={ ( event, editor ) => {
                                                        // console.log( 'Focus.', editor );
                                                    } }
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Keuntungan</Form.Label>
                                                <Form.Control as="textarea" style={{ height: '100px' }} name="benefits" value={benefits} onChange={this.handleChange}/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label htmlFor="minimumSalaryInlineFormInputGroup">Gaji minimum</Form.Label>
                                                <InputGroup className="mb-2">
                                                    <InputGroup.Text>Rp</InputGroup.Text>
                                                    <Form.Control  id="minimumSalaryInlineFormInputGroup" value={minSalary} name="minSalary" type="text" onChange={this.handleChange}/>
                                                </InputGroup>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group>
                                                <Form.Label htmlFor="maximumSalaryInlineFormInputGroup">Gaji maksimum</Form.Label>
                                                <InputGroup className="mb-2">
                                                    <InputGroup.Text>Rp</InputGroup.Text>
                                                    <Form.Control  id="maximumSalaryInlineFormInputGroup" value={maxSalary} name="maxSalary" type="text" onChange={this.handleChange}/>
                                                </InputGroup>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Tanggal berakhir <span className="text-6">*</span></Form.Label>
                                                <DatePicker
                                                    selected={endDate instanceof Date ? endDate : null}
                                                    name="endDate"
                                                    onChange={this.handleChangeEndDate}
                                                    dateFormat="dd MMMM yyyy"
                                                    showDatePicker
                                                    required
                                                />
                                                {typeof errors.endDate === "undefined"?
                                                    null 
                                                :
                                                    <div className="mt-2 text-danger font-size-14">
                                                        {errors.endDate}
                                                    </div>
                                                }
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="pb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Kontak <span className="text-6">*</span></Form.Label>
                                                <Form.Control value={contact} name="contact" type="text" placeholder="Kontak (mis: xxx@polban.ac.id)" onChange={this.handleChange} isInvalid={errors? (typeof errors.contact === "undefined"? false : errors.contact) : false} required/>
                                                <Form.Control.Feedback type="invalid" tooltip>
                                                    {errors? (typeof errors.contact === "undefined"? "" : errors.contact) : ""}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <hr></hr>
                                    <h2 className="mb-4 font-size-20 text-5">Requirement</h2>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Technical Skill <span className="text-6">*</span></Form.Label>
                                                <AsyncSelect
                                                    isMulti
                                                    cacheOptions
                                                    defaultOptions
                                                    loadOptions={this.loadSkillOptions}
                                                    onInputChange={this.handleInputSkillChange}
                                                    onChange={this.handleChangeSkills}
                                                    noOptionsMessage={() => null}
                                                    placeholder="Technical Skill (mis: Software Engineering)"
                                                    value={requirements? requirements.requiredSkills : null}
                                                    required
                                                />
                                                {typeof errors["requirements.requiredSkills"]=== "undefined"?
                                                    null 
                                                :
                                                    <div className="mt-2 text-danger font-size-14">
                                                        {errors["requirements.requiredSkills"]}
                                                    </div>
                                                }
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Soft Skill</Form.Label>
                                                <Form.Control as="textarea" style={{ height: '100px' }}  name="requirements" requirementname="softSkillRequirement" value={requirements? requirements.softSkillRequirement : null} onChange={this.handleChange}/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Usia maksimum</Form.Label>
                                                <Form.Control value={requirements.maximumAge? requirements.maximumAge : null} name="requirements" requirementname="maximumAge" type="text" onChange={this.handleChange}/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Tahun angkatan</Form.Label>
                                                <Select
                                                    isMulti
                                                    cacheOptions
                                                    defaultOptions
                                                    options={classYearOptions}
                                                    onChange={this.handleChangeClassYear}
                                                    noOptionsMessage={() => null}
                                                    placeholder={"Tahun angkatan (mis: " + currentYear + ")"}
                                                    value={requirements? requirements.classYearRequirement : null}
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Jenis kelamin</Form.Label>
                                                <div className="d-flex flex-row">
                                                    <Form.Check
                                                        inline
                                                        label="Laki-Laki"
                                                        name="requirements"
                                                        requirementname="requiredGender"
                                                        type="checkbox"
                                                        id="inline-gender-1"
                                                        className="d-flex align-items-center"
                                                        indexkey={0}
                                                        checked={gender[0]}
                                                        onChange={this.handleChange}
                                                    />
                                                    <Form.Check
                                                        inline
                                                        label="Perempuan"
                                                        name="requirements"
                                                        requirementname="requiredGender"
                                                        type="checkbox"
                                                        id="inline-gender-2"
                                                        className="d-flex align-items-center"
                                                        indexkey={1}
                                                        checked={gender[1]}
                                                        onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Agama</Form.Label>
                                                <div className="d-none d-lg-block">
                                                    <div className="d-flex flex-row">
                                                        <Form.Check
                                                            inline
                                                            label="Islam"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-1"
                                                            className="d-flex align-items-center"
                                                            indexkey={0}
                                                            checked={religion[0]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Kristen"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-2"
                                                            className="d-flex align-items-center"
                                                            indexkey={1}
                                                            checked={religion[1]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Buddha"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-3"
                                                            className="d-flex align-items-center"
                                                            indexkey={2}
                                                            checked={religion[2]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Hindu"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-4"
                                                            className="d-flex align-items-center"
                                                            indexkey={3}
                                                            checked={religion[3]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Katolik"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-5"
                                                            className="d-flex align-items-center"
                                                            indexkey={4}
                                                            checked={religion[4]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Khonghucu"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-6"
                                                            className="d-flex align-items-center"
                                                            indexkey={5}
                                                            checked={religion[5]}
                                                            onChange={this.handleChange}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="d-lg-none">
                                                    <div>
                                                        <Form.Check
                                                            inline
                                                            label="Islam"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-1"
                                                            className="d-flex align-items-center"
                                                            indexkey={0}
                                                            checked={religion[0]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Kristen"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-2"
                                                            className="d-flex align-items-center"
                                                            indexkey={1}
                                                            checked={religion[1]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Buddha"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-3"
                                                            className="d-flex align-items-center"
                                                            indexkey={2}
                                                            checked={religion[2]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Hindu"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-4"
                                                            className="d-flex align-items-center"
                                                            indexkey={3}
                                                            checked={religion[3]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Katolik"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-5"
                                                            className="d-flex align-items-center"
                                                            indexkey={4}
                                                            checked={religion[4]}
                                                            onChange={this.handleChange}
                                                        />
                                                        <Form.Check
                                                            inline
                                                            label="Khonghucu"
                                                            name="requirements"
                                                            requirementname="requiredReligion"
                                                            type="checkbox"
                                                            id="inline-religion-6"
                                                            className="d-flex align-items-center"
                                                            indexkey={5}
                                                            checked={religion[5]}
                                                            onChange={this.handleChange}
                                                        />
                                                    </div>
                                                </div>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Program studi</Form.Label>
                                                <div className="d-flex flex-row">
                                                    <Form.Check
                                                        inline
                                                        label="D3 Teknik Informatika"
                                                        name="requirements"
                                                        requirementname="studyProgramRequirement"
                                                        type="checkbox"
                                                        id="inline-study-program-1"
                                                        className="d-flex align-items-center"
                                                        indexkey={0}
                                                        checked={studyProgram[0]}
                                                        onChange={this.handleChange}
                                                    />
                                                    <Form.Check
                                                        inline
                                                        label="D4 Teknik Informatika"
                                                        name="requirements"
                                                        requirementname="studyProgramRequirement"
                                                        type="checkbox"
                                                        id="inline-study-program-2"
                                                        className="d-flex align-items-center"
                                                        indexkey={1}
                                                        checked={studyProgram[1]}
                                                        onChange={this.handleChange}
                                                    />
                                                </div>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label>Dokumen</Form.Label>
                                                <Form.Control value={requirements? requirements.documentRequirement : null} name="requirements" requirementname="documentRequirement" type="text" onChange={this.handleChange}/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4">
                                        <Col>
                                            <Form.Group>
                                                <Form.Label >Deskripsi</Form.Label>
                                                <CKEditor
                                                    editor={ ClassicEditor }
                                                    data={requirements.description}
                                                    onReady={ editor => {
                                                        // You can store the "editor" and use when it is needed.
                                                        // console.log( 'Editor is ready to use!', editor );
                                                    } }
                                                    onChange={ ( event, editor ) => {
                                                        const data = editor.getData();

                                                        this.setState(prevState => ({
                                                            formData: {
                                                                ...prevState.formData,
                                                                requirements: {
                                                                    ...prevState.formData.requirements,
                                                                    description: data
                                                                }

                                                            }
                                                        }));
                                                    } }
                                                    onBlur={ ( event, editor ) => {
                                                        // console.log( 'Blur.', editor );
                                                    } }
                                                    onFocus={ ( event, editor ) => {
                                                        // console.log( 'Focus.', editor );
                                                    } }
                                                />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className="d-flex">
                                            <div className="ml-auto">
                                                <Button className="button-pill bg-3" onClick={this.handleSave}>Simpan</Button>
                                            </div>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div></div>
            );
        }
    }
}

PostJob.propTypes = {
    getToken: PropTypes.func.isRequired
}
 
export default withRouter(PostJob);