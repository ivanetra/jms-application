import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";
import axios from 'axios';
import PropTypes from 'prop-types';
import { API_URL } from "../constants";
import { Col, Row, Dropdown, Pagination, Form, Badge} from "react-bootstrap";
import DatePicker from "react-datepicker";
 
class SearchedJobs extends Component {
  constructor(props) {
    super(props);

    let queryString = this.props.location.search.substring(1);
    let querySearchObject = {};
    if(queryString !== ""){
      querySearchObject = JSON.parse('{"' + queryString.replace(/&/g, '","').replace(/=/g,'":"') + '"}', function(key, value) { return key===""?value:decodeURIComponent(value) })
      
    }
    
    this.state = {
        searchedJobs: [],
        isLoaded: false,
        selectedClassYear: null,
        querySearch: {
          keyword: props.match.params.keyword,
          sort: querySearchObject.sort ? querySearchObject.sort : "",
          fJobType: querySearchObject.fJobType ? querySearchObject.fJobType : [],
          fStudyProgram: querySearchObject.fStudyProgram ? querySearchObject.fStudyProgram : [],
          fClassYear: querySearchObject.fClassYear ? querySearchObject.fClassYear : [],
          fRemoteStatus: querySearchObject.fRemoteStatus ? querySearchObject.fRemoteStatus : [],
          fMinSalary: querySearchObject.fMinSalary ? querySearchObject.fMinSalary : "",
          fMaxSalary: querySearchObject.fMaxSalary ? querySearchObject.fMaxSalary : "",
        },
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeSortFilter = this.handleChangeSortFilter.bind(this);
    this.handleChangeSortFilterArray = this.handleChangeSortFilterArray.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.getSearchResult = this.getSearchResult.bind(this);
  }

  getSearchResult(page, query){
    this.setState({
      isLoaded: false
    });

    const token = this.props.getToken();
    const {
      keyword = "",
      sort = "endDate",
      fJobType = [],
      fStudyProgram = [],
      fClassYear = [],
      fRemoteStatus = [],
      fMinSalary,
      fMaxSalary,
    } = query;
    let queryString= "?" +
    "sort=" + sort + "&" +
    "page=" + page + "&" +
    "keyword=" + keyword;

    if(fMinSalary && fMinSalary !== ""){      
      queryString += "&fMinSalary=" + fMinSalary;
    }
    if(fMaxSalary && fMaxSalary !== ""){      
      queryString += "&fMaxSalary=" + fMaxSalary;
    }
    
    fJobType.forEach((item) => {
      queryString += `&fJobType=${item}`;
    });
    fStudyProgram.forEach((item) => {
      queryString += `&fStudyProgram=${item}`;
    });
    fClassYear.forEach((item) => {
      queryString += `&fClassYear=${item}`;
    });
    fRemoteStatus.forEach((item) => {
      queryString += `&fRemoteStatus=${item}`;
    });
    
    let requestUrl = API_URL + "jobs/search" + queryString;
    
    console.log(requestUrl);
    
    axios.get(requestUrl, {
      headers: {
        accept: "application/json",
        authorization: "Bearer " + token
      }
    })
    .then(response => {
      let searchedJobs = response.data.result.searchResult;
      let pagination = response.data.result.pagination;

      this.setState({
        pagination,
        searchedJobs: searchedJobs,
        isLoaded: true
      })
    })
    .catch(err => {
      console.log(err);
    });
  }
  componentDidUpdate(prevProps){
    const locationChanged = this.props.location !== prevProps.location;
    // console.log(this.props.history);
  }
  componentDidMount() {
    this.getSearchResult(1, this.state.querySearch);
  }

  handleChange(object) {
    this.setState(object);
  }

  handleChangeSortFilter(key, value){
    let query = this.state.querySearch;
    query = {
      ...query, [key]:value
    }
    this.setState((prevState) => ({
      querySearch: {
        ...prevState.querySearch, [key]: value
      }
    }), this.getSearchResult(this.state.pagination.currentPage, query));

    
    console.log(this.props.history);
  }
  handleChangeSortFilterArray(key, value){
    let originalDate;

    if(key === "fClassYear"){
      if(value instanceof Date){
        originalDate = value;
        value = value.getFullYear();
      }else{
        originalDate = new Date();
        originalDate.setFullYear(value);
      }
    }


    //Add or remove filter
    let stateArray = this.state.querySearch[key];
    if(stateArray.includes(value)){
      stateArray.splice(stateArray.indexOf(value),1);
    }else{
      stateArray.push(value);
    }
    console.log(stateArray);

    //Set state
    let query = {
      ...this.state.querySearch, [key]: stateArray
    };
    if(key === "fClassYear"){
      this.setState((prevState) => ({
        selectedClassYear: originalDate,
        querySearch: {
          ...prevState.querySearch, [key]: stateArray
        }
      }), this.getSearchResult(this.state.pagination.currentPage, query));
    }else{
      this.setState((prevState) => ({
        querySearch: {
          ...prevState.querySearch, [key]: stateArray
        }
      }), this.getSearchResult(this.state.pagination.currentPage, query));

    }

    
    
    console.log(this.props.history);
  }
  handleChangePage(page){
    this.getSearchResult(page, this.state.querySearch);
  }

  render() {
    const { searchedJobs, isLoaded, pagination, querySearch, selectedClassYear } = this.state;
    let paginationItems = [];
    if(pagination){
      let active = pagination.currentPage;
      for(let number = 1; number <= pagination.totalPage; number++){
        paginationItems.push(
          <Pagination.Item key={number} active={number === active} onClick={() => {this.handleChangePage(number)}}>
            {number}
          </Pagination.Item>,
        );      
      }
    }
    let content;
        console.log(isLoaded)
        if (isLoaded && searchedJobs.length > 0) {
          // create pagination

          content = searchedJobs.map(searchedJob => {
            return (
                <li className="col-xl-3 col-lg-4 col-md-4 col-12 my-3" key={searchedJob.id}>
                  <NavLink to={{pathname:`/jobs/${searchedJob.id}`}} className="job w-100 h-100 card d-flex align-items-stretch">
                    <div className="card-body p-0">
                      <div className="row">
                        <div className="col-xl-8 col-lg-12 col-sm-12">
                          {searchedJob.companyLogo?
                            <img src={searchedJob.companyLogo} alt={searchedJob.title} width="48" height="48"></img>
                          :
                            <img src="/img/Company Logo.svg" alt={searchedJob.title} width="48" height="48"></img>
                          }
                        </div>
                        <div className="col-xl-9 col-lg-8 ">
                          <div className="job-card-title text-black">
                            {searchedJob.title}
                          </div>
                          {searchedJob.companyName?
                            <div className="job-card-text text-3 font-size-14">{searchedJob.companyName}</div>
                          :
                            null
                          }
                          {searchedJob.location? 
                            <div className="job-card-location text-3 font-size-14">{searchedJob.location}</div>
                          :
                            null
                          }
                        </div>
                      </div>
                    </div>
                    <div className="card-footer job-card-applicant-count border-0 p-0 bg-white font-size-12">
                      <div className="text-4">{searchedJob.applicantCount} Pelamar</div>
                      <div className="text-3">Diposting oleh {searchedJob.postedBy.name}</div>
                    </div>
                  </NavLink>
                </li>
              )
            }
          );
        } else if(!isLoaded){
          content=(<div>Loading ...</div>);
        } else{
          content="Tidak ada hasil pencarian yang sesuai."
        }
        return (
          <div className="bg-2 careers py-3" style={{minHeight: '90vh'}}>
              <div className="container-fluid">
                  <section className="job-boxes row root-padding mb-5">
                      <Row>
                        <Col xs={12} lg={3}>
                          <div className="card w-100 mt-4 pt-4">
                            <h4>Filter</h4>
                            {/* Filter Jenis Pekerjaan */}
                            <strong className="mt-3">Jenis Pekerjaan</strong>
                            <Form className="mt-3">
                              <Form.Group>
                                <Form.Check
                                    type="checkbox"
                                    label="Waktu Penuh"
                                    value={1}
                                    checked={querySearch.fJobType.includes("1")}
                                    onChange={(e) => {this.handleChangeSortFilterArray("fJobType", e.target.value)}}
                                />             
                                <Form.Check
                                    type="checkbox"
                                    label="Proyek"
                                    value={2}
                                    checked={querySearch.fJobType.includes("2")}                           
                                    onChange={(e) => {this.handleChangeSortFilterArray("fJobType", e.target.value)}}
                                />             
                              </Form.Group>
                            </Form>
                            {/* Filter Program Studi */}
                            <strong className="mt-3">Program Studi</strong>
                            <Form className="mt-3">
                              <Form.Group>
                                <Form.Check
                                    type="checkbox"
                                    label="D3-Teknik Informatika"
                                    value={1}
                                    checked={querySearch.fStudyProgram.includes("1")}                           
                                    onChange={(e) => {this.handleChangeSortFilterArray("fStudyProgram", e.target.value)}}
                                />             
                                <Form.Check
                                    type="checkbox"
                                    label="D4-Teknik Informatika"
                                    value={2}
                                    checked={querySearch.fStudyProgram.includes("2")}                           
                                    onChange={(e) => {this.handleChangeSortFilterArray("fStudyProgram", e.target.value)}}
                                />             
                              </Form.Group>
                            </Form>
                            {/* Filter Tahun Angkatan */}
                            <strong className="mt-3">Tahun Angkatan</strong>
                            <DatePicker
                              selected={selectedClassYear}
                              onChange={(date) => this.handleChangeSortFilterArray("fClassYear",date)}
                              showYearPicker
                              dateFormat="yyyy"
                              yearItemNumber={9}
                            />
                            <div>
                              {querySearch.fClassYear.length > 0 && querySearch.fClassYear.map((item) => {
                                return [
                                    <Badge className="mt-2" key={item} pill bg="primary">
                                      {item} &nbsp;
                                      <a href="#" className="button-circle p-0 text-white text-decoration-none" value={item} onClick={(e) => {e.preventDefault(); this.handleChangeSortFilterArray("fClassYear", item)}}>
                                      x
                                      </a>
                                    </Badge>
                                    , ' '
                                ]
                              })}
                            </div>
                            {/* Filter Range Gaji */}
                            <strong className="mt-3">Range Gaji</strong>
                            <Form className="mt-3">
                              <Form.Group>
                                <Form.Control size="sm" type="number" placeholder="Min" value={querySearch.fMinSalary} onChange={(e) => {this.handleChangeSortFilter("fMinSalary", e.target.value)}}/>      
                                <Form.Control className="mt-2" size="sm" type="number" placeholder="Max" value={querySearch.fMaxSalary} onChange={(e) => {this.handleChangeSortFilter("fMaxSalary", e.target.value)}} />           
                                     
                              </Form.Group>
                            </Form>

                            {/* Filter Remote */}
                            <strong className="mt-3">Status Remote</strong>
                            <Form className="mt-3">
                              <Form.Group>
                                <Form.Check
                                    type="checkbox"
                                    label="Remote"
                                    value={true}                                    
                                    checked={querySearch.fRemoteStatus.includes("true")}                           
                                    onChange={(e) => {this.handleChangeSortFilterArray("fRemoteStatus", e.target.value)}}
                                />      
                                <Form.Check
                                    type="checkbox"
                                    label="Tidak Remote"
                                    value={false}
                                    checked={querySearch.fRemoteStatus.includes("false")}                           
                                    onChange={(e) => {this.handleChangeSortFilterArray("fRemoteStatus", e.target.value)}}
                                />      
                              </Form.Group>
                            </Form>
                          </div>
                        </Col>
                        <Col>
                          <section className="card w-100 mt-4 pt-4">
                            <Dropdown className="ml-auto">
                              <Dropdown.Toggle variant="primary" id="dropdown-basic">
                                Sort berdasarkan
                              </Dropdown.Toggle>

                              <Dropdown.Menu>
                                <Dropdown.Item active={querySearch.sort === "endDate"} onClick={() => {this.handleChangeSortFilter("sort", "endDate")}}>Tanggal berakhir</Dropdown.Item>
                                <Dropdown.Item active={querySearch.sort === "salary"} onClick={() => {this.handleChangeSortFilter("sort", "salary")}}>Gaji tertinggi</Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                            <ul className="card-list row">
                              {content}
                            </ul>
                            {pagination && searchedJobs.length > 0 && (
                              <>
                                <Pagination size="sm">
                                  {paginationItems}
                                </Pagination>
                                <div className="text-3 ml-auto font-size-12">Menampilkan hasil pencarian ke <strong>{(pagination.currentPage-1)*pagination.itemPerPage+1}</strong> - <strong>{pagination.currentPage === pagination.totalPage ? pagination.totalItem:(pagination.currentPage)*pagination.itemPerPage}</strong> dari <strong>{pagination.totalItem}</strong> total hasil</div>
                              </>
                            )}
                          </section>     
                        </Col>
                      </Row>
                  </section>
              </div>
          </div>
        )
  }
}

SearchedJobs.propTypes = {
  getToken: PropTypes.func.isRequired
}
 
export default withRouter(SearchedJobs);