import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from 'axios';
import { API_URL } from "../constants";
import { Col, Container, Row, Button, ToastContainer } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExternalLinkSquareAlt } from "@fortawesome/free-solid-svg-icons";
import renderHTML from 'react-render-html';
import ApplyJobModal from "./ApplyJobModal";
import NotificationToast from "../components/NotificationToast";
import moment from 'moment';
 
class JobDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jobId: props.match.params.jobId,
            showApplyJobModal: false,
            notificationToast: [],
            job: {},
            isLoaded: false
        };
        this.getJobData = this.getJobData.bind(this);
        this.getUserData = this.getUserData.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleShowModal = this.handleShowModal.bind(this);
    }

    handleShowModal(e){
        this.setState({
            showApplyJobModal: true
        });
    }

    handleCloseModal(header, message){
        let {notificationToast} = this.state;
        const toastId = `${Math.random().toString(36).substr(2, 9)}`;
        if(message){
            notificationToast.push(
                <NotificationToast
                    id={toastId} 
                    key={toastId} 
                    header={header} body={message} show={true} 
                    onClose={() => {
                        let {notificationToast} = this.state;
                        notificationToast = notificationToast.filter((toast) => {console.log(toastId); return toast.props.id !== toastId});
                        this.setState({
                            notificationToast
                        })
                    }}
                />
            );
            this.setState({
                notificationToast,
                showApplyJobModal: false
            })
            
        }else{
            this.setState({
                showApplyJobModal: false
            })
        }
    }
    getUserData(userId){
        const token = this.props.getToken();
        axios.get(API_URL + `users/${userId}`, {
            headers: {
                accept: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            let recruiter = response.data.user;
            this.setState(prevState => ({
                job: {
                    ...prevState.job, recruiter
                },
                isLoaded: true
            }));

        })
        .catch(err => {
            console.log(err.response);
        });
    }

    getJobData(){
        const token = this.props.getToken();
        const {jobId} = this.state;
        axios.get(API_URL + `jobs/${jobId}`, {
            headers: {
                accept: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response)

            let job = response.data.result;

            if (job.companyLogo) {
                job.companyLogo = "https://api.jobmatcher.me/v1/file?filePath=" + job.companyLogo;
            }

            //Replace Null String with real Null
            Object.keys(job)
                .forEach((key) => {if(job[key] === "NULL") job[key] = null});

            this.setState({
                job, 
                isLoaded: false
            });
            
            this.getUserData(job.userId);
        })
        .catch(err => {
            console.log(err);
        });
    }

    componentDidMount() {
        const { jobId } = this.state;

        if(jobId){
            this.getJobData();
        }
    }
    formatRangeGaji(minSalary, maxSalary){
        if(minSalary == null) minSalary = NaN;
        if(maxSalary == null) maxSalary = NaN;
        minSalary = Number(minSalary);
        maxSalary = Number(maxSalary);

        var formatter = new Intl.NumberFormat('id-ID');
        
        let formattedMinSalary = `Rp ${formatter.format(minSalary)}`; 
        let formattedMaxSalary = `Rp ${formatter.format(maxSalary)}`; 
        if(isNaN(minSalary)){
            formattedMinSalary = "Tidak disebutkan";
        }
        if(isNaN(maxSalary)){
            formattedMaxSalary = "Tidak disebutkan";
        }
        if(isNaN(minSalary) && isNaN(maxSalary)){
            return "Tidak disebutkan";
        }
        return `${formattedMinSalary} - ${formattedMaxSalary}`
    }

    render() {
        const { job, isLoaded, jobId, showApplyJobModal, notificationToast } = this.state;
        
        if (isLoaded) {
            const {
                benefits,
                title,
                companyName,
                companyLogo,
                contact,
                description,
                duration,
                endDate,
                location,
                maximumAge,
                quantity,
                remote,
                requirements = {},
                jobType = {},
                minSalary,
                maxSalary,
                recruiter
            } = job;
            const {
                classYearRequirement,
                documentRequirement,
                softSkillRequirement,
                studyProgramRequirement,
                requiredGender,
                requiredReligion,
                requiredSkills,
            } = requirements;

            const salaryRequirement = this.formatRangeGaji(minSalary, maxSalary);

            return (
                <div className="bg-2 flex-grow-1 root-padding">
                    <ToastContainer className="p-3" position="top-start">
                        {notificationToast}
                    </ToastContainer>
                    <Container className="mb-5 bg-white w-100 px-4">
                        <Container className="row d-flex mx-0 px-0 pt-5">
                            <div className="col-xxl-3 col-lg-4 px-0 text-center">
                                <img src={companyLogo? companyLogo : "/img/Company Logo.svg"} alt={title} width="208" height="208"></img>
                            </div>
                            <div className="col-xxl-7 col-lg-8">
                                <h2 className="d-none d-lg-block font-weight-bold">{title}</h2>
                                <h2 className="d-lg-none text-center mt-2-5 font-weight-bold">{title}</h2>
                                <div className="d-none d-lg-block">
                                    {companyName? <span className="font-weight-medium font-size-20">{companyName}</span> : null}
                                    {companyName? 
                                        [
                                            (location? <span className="job-location font-size-20 font-weight-medium" key={location}>{location}</span> : null),
                                            (remote? <span className="job-remote font-size-20 font-weight-medium" key={remote}>Jarak jauh</span> : null)
                                        ]
                                    :
                                        [
                                            (location?
                                                [
                                                    (<span className="font-size-20 font-weight-medium" key={location}>{location}</span>),
                                                    (remote? <span className="job-remote font-size-20 font-weight-medium" key={remote}>Jarak jauh</span> : null)
                                                ]
                                            :
                                                (remote? <span className="font-size-20 font-weight-medium" key={remote}>Jarak jauh</span> : null)
                                            ),
                                        ]
                                    }
                                </div>
                                <div className="d-lg-none text-center">
                                    {companyName? <span className="font-weight-medium font-size-20">{companyName}</span> : null}
                                    {companyName? 
                                        [
                                            (location? <span className="job-location font-size-20 font-weight-medium" key={location}>{location}</span> : null),
                                            (remote? <span className="job-remote font-size-20 font-weight-medium" key={remote}>Jarak jauh</span> : null)
                                        ]
                                    :
                                        [
                                            (location? <span className="job-location font-size-20 font-weight-medium" key={location}>{location}</span> : null),
                                            (remote? <span className="job-remote font-size-20 font-weight-medium" key={remote}>Jarak jauh</span> : null)
                                        ]
                                    }
                                </div>
                                <h5 className="d-none d-lg-block mt-2 mb-0">{jobType}</h5>
                                <h5 className="d-lg-none mt-2 mb-0 text-center">{jobType}</h5>
                                <Button className="d-none d-lg-block button-circle mt-4 font-weight-bold px-4" style={{fontSize: "1.25em"}} size="lg" onClick={this.handleShowModal}>
                                    Lamar<FontAwesomeIcon className="ml-2" icon={faExternalLinkSquareAlt}/>
                                </Button>
                                <ApplyJobModal
                                    id={jobId}
                                    documentRequirement={documentRequirement}
                                    show={showApplyJobModal}
                                    handleClose={this.handleCloseModal} 
                                    getToken={this.props.getToken}
                                />
                                <div className="text-center">
                                    <Button className="d-lg-none button-circle mt-4 text-center font-weight-bold px-4" style={{fontSize: "1.25em"}} size="lg">Lamar<FontAwesomeIcon className="ml-2" icon={faExternalLinkSquareAlt}/></Button>
                                </div>
                            </div>
                        </Container>
                        <hr className="my-4"></hr>
                        <Container className="px-4">
                            <Row>
                                <h4 className="font-weight-bold">Detail</h4>
                            </Row>
                            <Row className="mb-5">
                                <p className="mb-0">Batas akhir lamaran: {endDate? moment(new Date(endDate)).format("DD MMMM YYYY") : null}</p>   
                                <p className="mb-0">Jumlah orang yang dibutuhkan: {quantity? quantity : null}</p>
                                {jobType === "Project" && duration?
                                    <p>Durasi proyek: {duration? duration : null}</p>
                                :
                                    null
                                }
                            </Row>
                            <Row>
                                <h5 className="font-weight-bold">Deskripsi Pekerjaan</h5>
                                <div className="mb-5">{description ? renderHTML(description) : "-"}</div>
                            </Row>
                            <Row className="mb-4">
                                <h5 className="font-weight-bold">Requirements</h5>
                                <Col xxl={3} lg={4} className="mb-3">
                                    <p className="font-weight-bold">Technical Skill</p>
                                    <ul className="mb-0">
                                        {requiredSkills && requiredSkills.map((skill) => {
                                            return (
                                                <li key={skill.id}>{skill.name}</li>
                                            );
                                        })}
                                    </ul>
                                </Col>
                                {softSkillRequirement?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Soft Skill</p>
                                        <p className="mb-0">{softSkillRequirement}</p>
                                    </Col> 
                                :
                                    null
                                }
                                {documentRequirement?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Dokumen</p>
                                        <p className="mb-0">{documentRequirement}</p>
                                    </Col> 
                                :
                                    null
                                }
                                {studyProgramRequirement.length !== 0?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Program Studi</p>
                                        {studyProgramRequirement.map((studyProgram, index) => (
                                            index === 0? <span>{studyProgram.name}</span> : <span>, {studyProgram.name}</span>
                                        ))}
                                    </Col> 
                                :
                                    null
                                }
                                {classYearRequirement.length !== 0?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Tahun Angkatan</p>
                                        {classYearRequirement.map((classYear, index) => (
                                            index === 0? <span>{classYear}</span> : <span>, {classYear}</span>
                                        ))}
                                    </Col> 
                                :
                                    null
                                }
                                {maximumAge?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Program Studi</p>
                                        <p className="mb-0">{maximumAge} Tahun</p>
                                    </Col> 
                                :
                                    null
                                }
                                {requiredGender.length !== 0?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Jenis Kelamin</p>
                                        {requiredGender.map((gender, index) => (
                                            index === 0? <span>{gender.name}</span> : <span>, {gender.name}</span>
                                        ))}
                                    </Col> 
                                :
                                    null
                                }
                                {requiredReligion.length !== 0?
                                    <Col xxl={3} lg={4} className="mb-3">
                                        <p className="font-weight-bold">Agama</p>
                                        {requiredReligion.map((religion, index) => (
                                            index === 0? <span>{religion.name}</span> : <span>, {religion.name}</span>
                                        ))}
                                    </Col> 
                                :
                                    null
                                }
                            </Row>
                            {requirements.description?
                                <Row>
                                    <Col>
                                        <div className="mb-5">{renderHTML(requirements.description)}</div>
                                    </Col>
                                </Row>
                            :
                                null
                            }
                            {benefits?
                                <Row>
                                    <Col>
                                        <h5 className="font-weight-bold">Benefits</h5>
                                        <p className="mb-5">{benefits}</p>
                                    </Col>
                                </Row>
                            : 
                                null
                            }
                            {salaryRequirement !== "Rp 0 - Rp 0"?
                                <Row>
                                    <Col>
                                        <h5 className="font-weight-bold">Gaji</h5>
                                        <p className="mb-5">{salaryRequirement}</p>
                                    </Col>
                                </Row>
                            :
                                null
                            }
                            <Row>
                                <Col>
                                    <h5 className="font-weight-bold">Kontak</h5>
                                    <p className="mb-5">{contact? contact : null}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <h4 className="font-size-16 text-3 font-weight-bold">Diposting oleh</h4>
                                    <p>{recruiter.name}</p>
                                </Col>
                            </Row>
                        </Container>
                    </Container>
                </div>
            );
        } else {
            return (
                <div className="bg-2 flex-grow-1 p-5">
                    <Container className="p-5 mb-5 bg-white">
                        <Row>
                            <h5>Loading...</h5>
                        </Row>
                    </Container>
                </div>
            );
        }
        
    }
}
 
export default withRouter(JobDetail);