import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import axios from 'axios';
import PropTypes from 'prop-types';
import { Form } from "react-bootstrap";
 
class Job extends Component {
  constructor(props) {
    super(props);
    this.state = {
        recommendedJobs: [],
        isLoaded: false,
        keyword: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const token = this.props.getToken();
    
    axios.get("https://api.jobmatcher.me/v1/jobs/recommendation", {
      headers: {
        accept: "application/json",
        authorization: "Bearer " + token
      }
    })
    .then(response => {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }

      let recommendedJobs = response.data.result;

      recommendedJobs.map(recommendedJob => {
        if (recommendedJob.job.companyLogo) {
          recommendedJob.job.companyLogo = "https://api.jobmatcher.me/v1/file?filePath=" + recommendedJob.job.companyLogo;
        }

        return recommendedJob;
      })

      this.setState({
        recommendedJobs: recommendedJobs,
        isLoaded: true
      })
    })
    .catch(err => {
      console.log(err);
    });
  }

  handleChange(object) {
    this.setState(object);
  }

  render() {
    const { recommendedJobs, isLoaded, errors } = this.state;
        if (isLoaded) {
          return (
              <div className="bg-2 careers pt-3 flex-grow-1">
                  <div className="container-fluid">
                      <section className="job-boxes row root-padding">
                        <nav className="job-home-nav card w-100 p-3">
                          <NavLink to="/posted-jobs" className="job-home-nav-item text-center text-black">Kelola posting pekerjaan</NavLink>
                        </nav>    
                        <section className="job-home-search-box-container card w-100 mt-4 px-4 pb-4 pt-3">
                          <h2 className="job-home-search-box-header text-center font-size-24">Cari pekerjaan</h2>
                          <div className="job-search-box-container mt-2">
                            <form onSubmit={this.handleSubmit} className="row">
                              <div className="col-lg-10 col-8 pt-0-5">
                                <Form>
                                  <Form.Group>
                                    <Form.Control className="job-search-box-text-input w-100 border-0 font-size-15" placeholder="Cari berdasarkan posisi pekerjaan, lokasi, atau nama perusahaan" isInvalid={errors? (typeof errors.keyword === "undefined"? false : errors.keyword) : false} type="text" value={this.state.keyword} onChange={(e) => this.handleChange({ keyword: e.target.value })}/>
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        {errors? (typeof errors.keyword === "undefined"? "" : errors.keyword) : ""}
                                    </Form.Control.Feedback>
                                  </Form.Group>
                                </Form>
                              </div>
                              <div className="col-lg-2 col-4 job-search-box-submit-button-container">
                                <NavLink 
                                  onClick={(e) => {
                                    console.log(e)
                                    if(this.state.keyword === undefined || this.state.keyword === "" ){
                                      e.preventDefault(); 
                                      this.setState({
                                        errors: {
                                          keyword: "Kata kunci pencarian harus diisi"
                                        }
                                      })
                                    }
                                  }} 
                                  to={{pathname: `/searched-jobs/${this.state.keyword}`}} 
                                  className="job-search-box-submit-button btn rounded-32 w-100 font-size-15 text-white"
                                >
                                  Cari
                                </NavLink>
                              </div>
                            </form>
                          </div>
                        </section> 
                        <section className="card w-100 mt-4 py-4">
                          {recommendedJobs.length !== 0?
                            [<div>
                                <div>
                                  <h2 className="job-module-header mb-0 font-size-18">Direkomendasikan untuk Anda</h2>
                                  <p className="mb-0 text-3 font-size-14">Berdasarkan profil dari keahlian Anda</p>
                                </div>
                                <div>
                                  <ul className="card-list row">
                                    {recommendedJobs.map(recommendedJob => (
                                      <li className="col-lg-3 col-12 my-3" key={recommendedJob.job.id}>
                                        <NavLink to={{pathname:`/jobs/${recommendedJob.job.id}`}} className="job w-100 h-100 card d-flex align-items-stretch">
                                          <div className="card-body p-0">
                                            <div className="row">
                                              <div className="col-lg-3 col-3">
                                                {recommendedJob.job.companyLogo?
                                                  <img src={recommendedJob.job.companyLogo} alt={recommendedJob.job.title} width="48" height="48"></img>
                                                :
                                                  <img src="/img/Company Logo.svg" alt={recommendedJob.job.title} width="48" height="48"></img>
                                                }
                                              </div>
                                              <div className="col-lg-9 col-9">
                                                <div className="job-card-title text-black">
                                                  {recommendedJob.job.title}
                                                </div>
                                                {recommendedJob.job.companyName?
                                                  <div className="job-card-text text-3 font-size-14">{recommendedJob.job.companyName}</div>
                                                :
                                                  null
                                                }
                                                {recommendedJob.job.location? 
                                                  <div className="job-card-location text-3 font-size-14">{recommendedJob.job.location}</div>
                                                :
                                                  null
                                                }
                                              </div>
                                            </div>
                                          </div>
                                          <div className="card-footer job-card-applicant-count border-0 p-0 bg-white font-size-12">
                                            <div className="text-4">{recommendedJob.applicantCount} Pelamar</div>
                                            <div className="text-3">Diposting oleh {recommendedJob.postedBy.name}</div>
                                          </div>
                                        </NavLink>
                                      </li>
                                    ))}
                                  </ul>
                                </div>
                            </div>]
                          :
                            [<div className="text-center">
                              Tidak ada lowongan pekerjaan yang dapat direkomendasikan
                            </div>]
                          }
                        </section>     
                      </section>
                  </div>
              </div>
          )
        } else {
          return <div></div>
        }
  }
}

Job.propTypes = {
  getToken: PropTypes.func.isRequired
}
 
export default Job;