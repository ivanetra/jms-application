import React, { Component } from "react";
import { 
    Modal,
    Button,
    Form,
    Row,
    Col
} from "react-bootstrap";
import {API_URL} from "../constants";
import axios from "axios";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ReactRoundedImage from "react-rounded-image";

class ManageApplicantModal extends Component{
    constructor(props) {
        super(props);
        this.state = {
            job: {
                id: props.jobId,
                title: props.jobTitle,
                companyName: props.jobCompanyName,
                location: props.jobLocation,
                contact: props.jobContact
            },
            applicant: {
                photo: props.applicantPhoto,
                name: props.applicantName
            },
            placeholder: "",
            formData: {
                applicantId: props.id,
                message: ""
            }
        };
        this.getPlaceholder = this.getPlaceholder.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeStartDate= this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    handleChange(e){
        let value = e.target.value;
        let name = e.target.name;


        if (this.state.errors) {
            if (this.state.errors.title) {
                if (name === "title") {
                    delete this.state.errors['title'];
                }
            }

            if (this.state.errors['workExperienceType.id']) {
                if (name === "workExperienceTypeId") {
                    delete this.state.errors['workExperienceType.id'];
                }
            }

            if (this.state.errors.companyName) {
                if (name === "companyName") {
                    delete this.state.errors['companyName'];
                }
            }
        }

        this.setState(prevState => ({
            formData:{
                ...prevState.formData, [name]: value
            }
        }));
    }

    handleChangeEndDate(endDate) {
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, endDate
            }
        }));
    }

    handleChangeStartDate(startDate){
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, startDate
            }
        }));
    }
    
    handleSave(){
        const { action } = this.props;
        const { id } = this.state.job;
        const { formData } = this.state;

        this.getPlaceholder();

        const token = this.props.getToken();

        if (action === "Layak") {
            //call api untuk terima applicant
            axios.post(API_URL + `jobs/${id}/applicants/accept`, formData, {
                headers: {
                    accept: "*/*",
                    contentType: "application/json",
                    authorization: "Bearer " + token
                }
            })
            .then(response => {  
                console.log(response.data);
    
                if (response.data.result === "Success") {
                    window.location.reload();
                } else {
                    this.setState({
                        errors: response.data.result.errors
                    })
                }
            })
            .catch((error) => {
                console.log(error);
            })
        } else {
            //call api untuk tolak applicant
            axios.post(API_URL + `jobs/${id}/applicants/refuse`, formData, {
                headers: {
                    accept: "*/*",
                    contentType: "application/json",
                    authorization: "Bearer " + token
                }
            })
            .then(response => {  
                console.log(response.data);
    
                if (response.data.result === "Success") {
                    window.location.reload();
                } else {
                    this.setState({
                        errors: response.data.result.errors
                    })
                }
            })
            .catch((error) => {
                console.log(error);
            })
        }
    }

    handleDelete(){
        //call api untuk menambahkan pengalaman kerja
        const {workExperienceId} = this.state;

        const token = this.props.getToken();
        axios.delete(API_URL + `work-experience/${workExperienceId}`, {
            headers: {
              contentType: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response.data);
            this.setState({
                formData: {}
            });
            this.props.handleClose("Data pengalaman kerja berhasil dihapus.");
            window.location.reload();
        })
        .catch(error => {
            console.log(error);
        })
    }

    getWorkExperienceTypes(){
        const token = this.props.getToken();
        axios.get(API_URL + 'work-experience-type', {
            headers: {
              accept: "application/json",
              authorization: "Bearer " + token
            }
        })
        .then(response => {
            this.setState({
                workExperienceTypes: response.data.result,
                isLoaded: true
            });
        })
        .catch(error => {
            console.log(error);
        })
    }

    getPlaceholder() {
        const { action } = this.props;
        const {applicant, job} = this.state;

        let placeholder = "";

        if (action === "Layak") {
            placeholder = applicant.name + ", Terima kasih atas ketertarikannya untuk posisi " + job.title;
                    
            if (job.companyName) {
                placeholder += " pada " + job.companyName;
    
                if (job.location) {
                    placeholder += " di " + job.location;
                }
            }
    
            placeholder += ". Setelah mempelajari kualifikasi dari saudara secara keseluruhan, kami sangat tertarik dengan kemampuan yang dimiliki saudara. Dengan demikian kami berharap saudara dapat mengikuti proses wawancara, mengenai waktu dan tempat akan kami informasikan lebih lanjut. Salam, ";
    
            if (job.companyName) {
                placeholder += job.companyName;
            } else {
                placeholder += job.contact;
            }
    
            placeholder += ".";
        } else {
            placeholder = applicant.name + ", Terima kasih atas ketertarikannya untuk posisi " + job.title;
                    
            if (job.companyName) {
                placeholder += " pada " + job.companyName;

                if (job.location) {
                    placeholder += " di " + job.location;
                }
            }
    
            placeholder += ". Sayangnya, kami tidak akan menindaklanjuti lamaran anda. Kami sangat mengapresiasi waktu dan ketertarikan anda";
    
            if (job.companyName) {
                placeholder += " pada " + job.companyName;
            }

            placeholder += ". Salam, ";
                
            if (job.companyName) {
                placeholder += job.companyName;
            } else {
                placeholder += job.contact;
            }
    
            placeholder += ".";            
        }
        
        this.setState({placeholder: placeholder});
    }

    componentDidMount(){
        this.getPlaceholder();
    }

    componentDidUpdate(prevProps) {
        
        if (this.props.show !== prevProps.show) {
            
            this.setState(prevState => ({
                formData: {
                    ...prevState.formData,
                    applicantId: this.props.id,
                },
                jobId: this.props.jobId
            }), () => {this.getPlaceholder()})
        }
    }

    render(){
        // if(!(startDate instanceof Date)) startDate = new Date(startDate);
        // if(!(endDate instanceof Date)) endDate = new Date(endDate);
        const { placeholder } = this.state;
        const { photo, name } = this.state.applicant;
        const { message } = this.state.formData;
        const {props} = this;
        return (
            <>                
                <Modal
                    show={props.show}
                    onHide={() => {delete this.state['errors']; props.handleClose()}}
                    size="lg"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.action}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                    <Form>
                        <Row>
                            <Col lg={1}>
                                <ReactRoundedImage image={photo? photo : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="48" imageHeight="48" roundedSize="0" className="applicant-photo" alt={name}/>
                            </Col>
                            <Col lg={11}>
                                <div className="font-weight-bold">{name}</div>
                                {props.action === "Layak"?
                                    <div className="text-4">{this.props.action}</div>
                                :
                                    <div className="text-2">{this.props.action}</div>
                                }
                            </Col>
                        </Row>
                        <Form.Group className="mt-3">
                            <CKEditor
                                editor={ ClassicEditor }
                                config={{placeholder: placeholder}} 
                                data={message}
                                onReady={ editor => {
                                    // You can store the "editor" and use when it is needed.
                                    // console.log( 'Editor is ready to use!', editor );
                                } }
                                onChange={ ( event, editor ) => {
                                    const data = editor.getData();
                                    
                                    this.setState(prevState => ({
                                        formData: {
                                            ...prevState.formData,
                                            message: data
                                        }
                                    }));
                                } }
                                onBlur={ ( event, editor ) => {
                                    console.log( 'Blur.', editor );
                                } }
                                onFocus={ ( event, editor ) => {
                                    console.log( 'Focus.', editor );
                                } }
                            />
                        </Form.Group>
                    </Form>
                    </Modal.Body>

                    <Modal.Footer>
                        <div className="ml-auto">
                            <Button className="button-pill border-0" variant="outline-primary" onClick={() => props.handleClose()}>Jangan kirim</Button>
                        </div>
                        <div>
                            <Button className="button-pill" variant="primary" onClick={this.handleSave}>Simpan</Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default ManageApplicantModal;