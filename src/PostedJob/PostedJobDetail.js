import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";
import axios from 'axios';
import NotificationToast from "../components/NotificationToast";
import ManageApplicantModal from "../Applicant/ManageApplicantModal";
import { Form } from "react-bootstrap";
import ReactRoundedImage from "react-rounded-image";
 
class PostedJobDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jobId: props.match.params.jobId,
            job: {},
            applicantStatus: [0],
            isLoaded: false,
            showModals: {
                acceptApplicant: [false],
                refuseApplicant: [false]
            },
            notificationToast: []
        };

        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        const token = this.props.getToken();

        axios.get("https://api.jobmatcher.me/v1/jobs/" + this.state.jobId, {
            headers: {
                accept: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }

            let applicantStatus = [];
            let acceptApplicantModals = [];
            let job = response.data.result;

            return axios.get("https://api.jobmatcher.me/v1/jobs/" + this.state.jobId + "/applicants/recommendation", {
                headers: {
                    accept: "application/json",
                    authorization: "Bearer " + token
                }
            })
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }

                job.applicants = response.data.applicants;

                if(job.applicants.length) {
                    return Promise.all(job.applicants.map(applicant => {
                        applicantStatus.push(applicant.applicant.status);
                        return axios.get("https://api.jobmatcher.me/v1/users/" + applicant.applicant.user.nim + "/skills", {
                            headers: {
                                accept: "application/json",
                                authorization: "Bearer " + token
                            }
                        })
                        .then(response => {
                            if (response.status >= 400) {
                                throw new Error("Bad response from server");
                            }
                            
                            
                            applicant.applicant.user.skills = response.data.userSkills;
                            return job;
                        })
                        .then(job => this.setState(prevState => ({
                            job: job,
                            isLoaded: true
                        })))
                        .then(() => {
                            acceptApplicantModals.push(false);
                            
                        })
                    }))
                } else {
                    this.setState({
                        job: job,
                        isLoaded: true
                    })
                }
            })
            .then(() => {
                this.setState(prevState => ({
                    applicantStatus: applicantStatus,
                    showModals: {
                        ...prevState.showModals,
                        acceptApplicant: acceptApplicantModals
                    }
                }))
            })
        })
        .catch(err => {
            console.log(err);
        });
    }

    handleChange(event) {
        const applicantIndex = Number(event.currentTarget.getAttribute("indexkey"));
        const status = Number(event.currentTarget.value);

        this.setState(prevState => ({
            job: {
                ...prevState.job,
                applicants: prevState.job.applicants.map((applicant, index) => (
                    index === applicantIndex? Object.assign(applicant, {...applicant, applicant: {...applicant.applicant, status: status}}) : applicant
                ))
            }
        }));
    }

    handleCloseModal(message, modalName){
        let {notificationToast} = this.state;

        const toastId = `${Math.random().toString(36).substr(2, 9)}-${modalName}`;

        if(message){
            notificationToast.push(
                <NotificationToast 
                    id={toastId} 
                    key={toastId} 
                    header="Berhasil!" body={message} show={true} 
                    onClose={() => {
                        let {notificationToast} = this.state;
                        notificationToast = notificationToast.filter((toast) => {console.log(toastId); return toast.props.id !== toastId});
                        this.setState({
                            notificationToast
                        })
                    }}
                />
            );
            this.setState(prevState => ({
                notificationToast,
                showModals: {
                    ...prevState.showModals, [modalName]: false
                }
            }));
        }else{
            this.setState(prevState => ({
                showModals: {
                    ...prevState.showModals, [modalName]: false
                }
            }));

        }
    }

    handleShowModal(event){
        const key = event.currentTarget.getAttribute('indexkey');
        const status = Number(event.currentTarget.value);

        let modalName;

        if (status === 1) {
            modalName = "acceptApplicant";
        } else if (status === 2) {
            modalName = "refuseApplicant";
        }

        this.setState(prevState => ({
            showModals: {
                ...prevState.showModals, [modalName]: {
                    [key]: true
                }
            }
        }));
    }

    render() {
        const { job, applicantStatus, isLoaded } = this.state;
        const { acceptApplicant, refuseApplicant } = this.state.showModals;

        if (isLoaded) {
            return (
                <div className="bg-2 flex-grow-1 mb-5 pb-4">
                    <div className="container-fluid">
                        <div className="grid row">
                            <div className="job-top-card-info d-flex flex-column w-100 bg-white py-2-5 root-padding">
                                <div className="row">
                                    <div className="col-lg-8 col-9 d-flex justify-content-center flex-column">
                                        <div>
                                            <h1 className="mb-0 font-size-16 text-5 font-weight-semi-bold">{job.title}</h1>
                                        </div>
                                        {job.companyName || job.location? 
                                            <div className="font-size-14">
                                                {job.companyName?
                                                    <span className="job-top-card-info-item text-5" id="company-name">{job.companyName}</span>
                                                :
                                                    "-"
                                                }
                                                {job.location !== ""? 
                                                    <span className="job-top-card-info-item text-5" id="job-location">{job.location}</span>
                                                :
                                                    "-"
                                                }
                                            </div>
                                        :
                                            null
                                        }
                                    </div>
                                    <div className="job-top-card-action d-flex col-lg-4 col-3 btn-group ml-auto">
                                        <NavLink to={{pathname:`/edit-posted-job/${job.id}`}} className="d-none d-lg-block btn rounded-32 bg-3 px-3 text-white font-weight-semi-bold" type="button">Kelola pekerjaan</NavLink>
                                        <button className="btn d-none d-lg-block ml-3 bg-white dropdown-toggle align-items-center" data-toggle="dropdown" aria-expanded="false">
                                            <img className="ellipsis-horizontal-icon" src="/img/Ellipsis Horizontal.svg" alt="Ellipsis Horizontal" width="24" height="24"></img>
                                        </button>
                                        <button className="btn d-lg-none ml-3 bg-white navbar-toggler align-items-center" data-toggle="collapse" data-target="#jobTopCardInfoSupportedContent" aria-controls="jobTopCardInfoSupportedContent" aria-expanded="false" aria-label="Toogle Navigation">
                                            <img className="ellipsis-horizontal-icon" src="/img/Ellipsis Horizontal.svg" alt="Ellipsis Horizontal" width="24" height="24"></img>
                                        </button>
                                        <ul className="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <NavLink to={'/jobs/' + job.id}className="dropdown-item px-3 font-weight-semi-bold text-3" id="view-as-candidate" type="button">
                                                    <img className="view-as-candidate-icon mr-1" src="/img/View.svg" alt="View as candidate" width="24" height="24"></img>
                                                    Lihat sebagai kandidat
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink to='/posted-jobs' className="dropdown-item px-3 font-weight-semi-bold text-3" id="see-all-my-job-posts" type="button">
                                                    <img className="see-all-my-job-posts-icon mr-1" src="/img/See All My Job Posts.svg" alt="See All My Job Posts Icon" width="24" height="24"></img>
                                                    Lihat semua posting pekerjaan saya
                                                </NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="row collapse navbar-collapse mt-4 " id="jobTopCardInfoSupportedContent">
                                    <div className="col-lg-12 col-12">
                                        <hr className="mt-0"></hr>
                                        <ul className="list-unstyled">
                                            <li className="text-center">
                                                <NavLink to={{pathname:`/edit-posted-job/${job.id}`}} className="btn rounded-32 bg-3 px-3 text-white font-weight-semi-bold" type="button">Kelola pekerjaan</NavLink>
                                            </li>
                                            <hr></hr>
                                            <li className="mt-1">
                                                <NavLink to={'/jobs/' + job.id}className="dropdown-item px-3 text-center font-weight-semi-bold text-3" id="view-as-candidate" type="button">
                                                    Lihat sebagai kandidat
                                                </NavLink>
                                            </li>
                                            <hr></hr>
                                            <li>
                                                <NavLink to='/posted-jobs' className="dropdown-item px-3 text-center font-weight-semi-bold text-3" id="see-all-my-job-posts" type="button">
                                                    Lihat semua posting pekerjaan saya
                                                </NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="root-padding w-100">
                                <div className="w-100">
                                    <div className="numbers-of-applicant bg-white py-2-5 px-4-3 text-5 font-weight-semi-bold">
                                        {job.applicants.length} Pelamar
                                    </div>
                                </div>
                                <div className="applicant-job-detail  w-100">
                                    {job.applicants.map((applicant, applicantIndex) => (
                                        <div className="applicant-job bg-white py-2-5 px-4-3 mx-0" key={applicant.applicant.user.nim}>
                                            <div className="row">
                                                <div className="col-lg-1 col-2 px-0">
                                                    <ReactRoundedImage image={applicant.applicant.user.photo? applicant.applicant.user.photo : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="48" imageHeight="48" roundedSize="0" className="applicant-photo" alt={applicant.applicant.user.name}/>
                                                </div>
                                                <div className="col-lg-11 col-10">
                                                    <div className="row">
                                                        <div className="col-lg-10 col-6 d-flex align-items-center">
                                                            <NavLink to={{pathname: '/profile/' + applicant.applicant.user.nim}} className="text-decoration-none">
                                                                <h2 className="mb-0 font-size-16 text-5 font-weight-semi-bold">{applicant.applicant.user.name}</h2>
                                                            </NavLink>
                                                        </div>
                                                        <div className="col-lg-2 col-6">
                                                            {applicantStatus[applicantIndex]?
                                                                <Form.Select className="form-select" aria-label="-" name="gender" value={applicant.applicant.status} indexkey={applicantIndex} onChange={this.handleChange} onClick={this.handleShowModal} disabled>
                                                                    <option value={0}>-</option>
                                                                    <option value={1}>Layak</option>
                                                                    <option value={2}>Tolak</option>
                                                                </Form.Select>
                                                            :
                                                                <Form.Select className="form-select" aria-label="-" name="gender" value={applicant.applicant.status} indexkey={applicantIndex} onChange={this.handleChange} onClick={this.handleShowModal}>
                                                                    <option value={0}>-</option>
                                                                    <option value={1}>Layak</option>
                                                                    <option value={2}>Tolak</option>
                                                                </Form.Select>
                                                            }
                                                            <ManageApplicantModal  
                                                                id={applicant.applicant.user.nim}
                                                                applicantPhoto={applicant.applicant.user.photo}
                                                                applicantName={applicant.applicant.user.name}
                                                                jobId={job.id}
                                                                jobTitle={job.title}
                                                                jobCompanyName={job.companyName}
                                                                jobLocation={job.location}
                                                                jobContact={job.contact}
                                                                name="acceptApplicant"
                                                                show={acceptApplicant[applicantIndex]}
                                                                handleClose={(message) => {
                                                                    this.handleCloseModal(message, "acceptApplicant"); 
                                                                            
                                                                    this.setState(prevState => ({
                                                                        job: {
                                                                            ...prevState.job,
                                                                            applicants: prevState.job.applicants.map((applicant, index) => (
                                                                                index === applicantIndex? Object.assign(applicant, {...applicant, applicant: {...applicant.applicant, status: 0}}) : applicant
                                                                            ))
                                                                        }
                                                                    }));
                                                                }}
                                                                getToken={this.props.getToken}
                                                                action="Layak"
                                                            />
                                                            <ManageApplicantModal  
                                                                id={applicant.applicant.user.nim}
                                                                applicantPhoto={applicant.applicant.user.photo}
                                                                applicantName={applicant.applicant.user.name}
                                                                jobId={job.id}
                                                                jobTitle={job.title}
                                                                jobCompanyName={job.companyName}
                                                                jobLocation={job.location}
                                                                jobContact={job.contact}
                                                                name="refuseApplicant"
                                                                show={refuseApplicant[applicantIndex]}
                                                                handleClose={(message) => {
                                                                    this.handleCloseModal(message, "refuseApplicant"); 
                                                                            
                                                                    this.setState(prevState => ({
                                                                        job: {
                                                                            ...prevState.job,
                                                                            applicants: prevState.job.applicants.map((applicant, index) => (
                                                                                index === applicantIndex? Object.assign(applicant, {...applicant, applicant: {...applicant.applicant, status: 0}}) : applicant
                                                                            ))
                                                                        }
                                                                    }));
                                                                }}
                                                                getToken={this.props.getToken}
                                                                action="Tolak"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="skills row container-fluid mt-2-5">
                                                        {applicant.applicant.user.skills?
                                                            applicant.applicant.user.skills.map(skill => (
                                                                <div className="d-none d-lg-block col-lg-4 mx-1 my-1 btn border-2 rounded-32 bg-white px-3 text-1" key={skill.id}>{skill.name}</div>
                                                            ))
                                                        :
                                                            null
                                                        }
                                                        {applicant.applicant.user.skills?
                                                            applicant.applicant.user.skills.map(skill => (
                                                                <div className="d-lg-none col-lg-4 col-12 mx-1 my-1 btn border-2 rounded-32 bg-white px-3 font-size-12 text-1" key={skill.id}>{skill.name}</div>
                                                            ))
                                                        :
                                                            null
                                                        }
                                                    </div>
                                                    <div className="mt-3">
                                                        <div className="col-12 ml-auto text-3 font-size-12">Dikirim pada {applicant.applicant.dateApplied}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <div></div>
        }
        
    }
}
 
export default withRouter(PostedJobDetail);