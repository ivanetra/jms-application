import { Component } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from 'prop-types';
import axios from 'axios';
import { Modal, Button } from "react-bootstrap";
import NotificationToast from "../components/NotificationToast";
import {API_URL} from "../constants";

class PostedJob extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postedJobs: [],
            isLoaded: false,
            showModals: [],
            notificationToast: []
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    componentDidMount() {
        const nim = this.props.getNim();
        const token = this.props.getToken();

        let modals = [];

        axios.get("https://api.jobmatcher.me/v1/jobs?userId=" + nim, {
            headers: {
                accept: "application/json",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            console.log(response)
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }

            modals.map(() => {
                return modals.push(false);
            })

            this.setState({
                postedJobs: response.data.result,
                showModals: modals,
                isLoaded: true
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    handleDelete(event) {
        const id = event.currentTarget.getAttribute('jobid');
        const token = this.props.getToken();

        //call api untuk menghapus lowongan pekerjaan yang diposting
        axios.delete(API_URL + `jobs/${id}`,{
            headers: {
                accept: "*/*",
                authorization: "Bearer " + token
            }
        })
        .then(response => {
            window.location.reload();
        })
        .catch(error => {
            console.log(error);
        })
    }

    handleCloseModal(message, modalName, key) {
        let {notificationToast} = this.state;
        const toastId = `${Math.random().toString(36).substr(2, 9)}-${modalName}`;
        if(message){
            notificationToast.push(
                <NotificationToast 
                    id={toastId} 
                    key={toastId} 
                    header="Berhasil!" body={message} show={true} 
                    onClose={() => {
                        let {notificationToast} = this.state;
                        notificationToast = notificationToast.filter((toast) => {console.log(toastId); return toast.props.id !== toastId});
                        this.setState({
                            notificationToast
                        })
                    }}
                />
            );
            this.setState(prevState => ({
                notificationToast,
                showModals: {
                    ...prevState.showModals,
                    [key]: false
                }
            }));
        }else{
            this.setState(prevState => ({
                showModals: {
                    ...prevState.showModals, 
                    [key]: false
                }
            }));
        }
    }

    handleShowModal(event) {
        const key = event.currentTarget.getAttribute('indexkey');

        this.setState(prevState => ({
            showModals: {
                    ...prevState.showModals,
                    [key]: true
                }
            })
        );
    }

    render() {
        const { postedJobs, showModals, isLoaded } = this.state;
        if (isLoaded) {
            return (
                <div className="bg-2 pt-4 flex-grow-1">
                    <div className="container-fluid">
                        <div className="grid row root-padding">
                            <div className="col-lg-4 d-lg-none p-0">
                                <aside className="result-container-right card w-100 p-3">
                                    <NavLink to="/post-job" className="post-job btn bg-white text-1 font-weight-600">
                                        <img className="compose-icon mr-1" src="/img/Compose.svg" alt="Compose" width="16" height="16"></img>
                                        Posting pekerjaan
                                    </NavLink>
                                </aside>
                            </div>
                            <div className="col-lg-8 p-0">
                                <section className="result-container-left card w-100 p-0">
                                    <h3 className="posted-job-header mb-0 pt-3 px-4 font-size-24">Pekerjaan yang Diposting</h3>
                                    <hr className="divider mb-0"/>
                                    <ul className="result-list list-unstyled mb-0">
                                        {postedJobs.map((postedJob, index) => (
                                            <li className="result-container" key={postedJob.id}>
                                                <div className="entity-result-item d-flex p-3">
                                                    <NavLink to={{pathname:`/posted-job-detail/${postedJob.id}`}} className="entity-result-content">
                                                        <div className="text-5">
                                                            <strong>{postedJob.title}</strong>
                                                        </div>
                                                        <div className="entity-result-primary-subtitle font-size-14 text-5">
                                                            {postedJob.companyName? postedJob.companyName : "-"}
                                                        </div>
                                                    </NavLink>
                                                    <div className="entity-result-action ml-auto btn-group">
                                                        <button className="dropdown-trigger btn bg-white dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                            <img className="ellipsis-horizontal-icon" src="/img/Ellipsis Horizontal.svg" alt="Ellipsis Horizontal" width="24" height="24"></img>
                                                        </button>
                                                        <ul className="dropdown-menu dropdown-menu-right">
                                                            <li>
                                                                <button className="dropdown-item px-2" id="deleteJob" type="button" indexkey={index} onClick={this.handleShowModal}>
                                                                    <img className="delete-job-icon mr-1" src="/img/Delete.svg" alt="Delete Job Icon" width="24" height="24"></img>
                                                                    Hapus pekerjaan
                                                                </button>
                                                                <Modal show={showModals[index]} onHide={(message) => this.handleCloseModal(message, "job", index)}>
                                                                    <Modal.Header closeButton>Hapus pekerjaan</Modal.Header>
                                                                    <Modal.Body>Anda tidak akan bisa mengakses pekerjaan ini lagi setelah dihapus. Apakah Anda yakin Anda ingin menghapus pekerjaan ini?</Modal.Body>
                                                                    <Modal.Footer>
                                                                        <Button className="cancel-button rounded-24 border-2 bg-white text-1 font-weight-semi-bold" onClick={(message) => this.handleCloseModal(message, "job", index)}>Batalkan</Button>
                                                                        <Button className="delete-button rounded-24 bg-3 font-weight-semi-bold" jobid={postedJob.id} onClick={this.handleDelete}>Hapus</Button>
                                                                    </Modal.Footer>
                                                                </Modal>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        ))}
                                    </ul>
                                </section>
                            </div>
                            <div className="d-none d-lg-block col-lg-4">
                                <aside className="result-container-right card w-100 p-3">
                                    <NavLink to="/post-job" className="post-job btn bg-white text-1 font-weight-600">
                                        <img className="compose-icon mr-1" src="/img/Compose.svg" alt="Compose" width="16" height="16"></img>
                                        Posting pekerjaan
                                    </NavLink>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <div></div>
        }
    }
}

PostedJob.propTypes = {
    getNim: PropTypes.func.isRequired,
    getToken: PropTypes.func.isRequired
}
 
export default PostedJob;