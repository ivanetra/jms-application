import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import axios from "axios";
import { Form, InputGroup } from "react-bootstrap";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        nim: "",
        email: "",
        password: "",
      },
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e){
    let value = e.target.value;
    let name = e.target.name;

    if (this.state.errors) {
        if (this.state.errors.nim) {
            if (name === "nim") {
                delete this.state.errors['nim'];
            }
        }
        
        if (this.state.errors.email) {
            if (name === "email") {
                delete this.state.errors['email'];
            }
        }

        if (this.state.errors.password) {
            if (name === "password") {
                delete this.state.errors['password'];
            }
        }
    }

    if (name === "nim") {
        if (/^\d+$/.test(value) || value === "") {
          if (value.length <= 9) {
            this.setState(prevState => ({
              formData:{
                  ...prevState.formData, [name]: value
              }
            }));
          }
        }
    } else {
        this.setState(prevState => ({
            formData:{
                ...prevState.formData, [name]: value
            }
        }));
    }
}

  handleSubmit(event) {
    event.preventDefault();

    const { formData } = this.state;

    axios.post("https://api.jobmatcher.me/v1/auth/register", formData, {
      headers: {
        accept: "application/json"
      }
    })
    .then(response => {
      console.log(response);
      
      const result = response.data.result;

      if (result.errors) {
        this.setState({
            errors: result.errors
        })
      } else {
        const { email, password } = this.state.formData;
        
        this.props.history.push({pathname: '/verification', state: {email: email, password: password}});
      }
    })
    .catch(error => {
      console.log(error.response);
      const errors = error.response.data.errors;
      const message = error.response.data.message;

      if (errors) {
        this.setState({errors: errors});
      }

      if(message) {
        if (message === "NIM sudah terdaftar") {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              nim: message
            }
          }))
        } else if (message === "Email sudah terdaftar") {
            this.setState(prevState => ({
                errors: {
                  ...prevState.errors,
                  email: message
                }
            }))
        }else {
          this.setState(prevState => ({
            errors: {
              ...prevState.errors,
              password: message
            }
          }))
        }
      }
    });
  }

  render() {
    const { errors } = this.state;
    const { nim, email, password } = this.state.formData;

    return (
        <div className="bg-2 flex-grow-1">
            <div className="container-fluid">
                <section className="section-signup">
                  <div className="text-center">
                    <NavLink to="/" className="jms-logo text-center text-1"><b>JMS</b></NavLink>
                  </div>
                  <div className="card mt-4">
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label className="mb-1 font-size-14 text-3">Nomor induk mahasiswa</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Control value={nim} name="nim" type="text" onChange={this.handleChange} isInvalid={errors? (typeof errors.nim === "undefined"? false : errors.nim) : false} required autoFocus/>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors? (typeof errors.nim === "undefined"? "" : errors.nim) : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>
                        <Form.Group className="mt-3">
                            <Form.Label className="mb-1 font-size-14 text-3">Email</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Control value={email} name="email" type="email" onChange={this.handleChange} isInvalid={errors? (typeof errors.email === "undefined"? false : errors.email) : false} required/>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors? (typeof errors.email === "undefined"? "" : errors.email) : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>
                        <Form.Group className="mt-3">
                            <Form.Label className="mb-1 font-size-14 text-3">Kata sandi (sedikitnya 6 karakter)</Form.Label>
                            <InputGroup hasValidation>
                                <Form.Control value={password} name="password" type="password" onChange={this.handleChange} isInvalid={errors? (typeof errors.password === "undefined"? false : errors.password) : false} required/>
                                <Form.Control.Feedback type="invalid" tooltip>
                                    {errors? (typeof errors.password === "undefined"? "" : errors.password) : ""}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>
                        <button className="signup-action mt-3 w-100" type="submit">Bergabung</button>
                        <div className="mt-3 text-center">
                            Sudah bergabung di JMS?
                            <NavLink to="/login" id="login" className="login-now ml-2 text-1">Login</NavLink>
                        </div>   
                    </Form>
                  </div>        
                </section>
            </div>
        </div>
    );
  }
}
 
export default withRouter(Signup);