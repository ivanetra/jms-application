import React, { Component } from "react";
import {
    NavLink
  } from "react-router-dom";
 
class Home extends Component {
  render() {
    return (
        <div>
            <div className="container-fluid">
                <section className="section-welcome row root-padding">
                    <div className="section-welcome-left col-lg-6">
                        <h1 className="welcome-headline">Selamat datang di Job Matching System</h1>
                        <ul className="welcome-module">
                            <li className="welcome-module-list">
                                <NavLink to="/login" className="welcome-module-button">
                                    Temukan lowongan kerja dan proyek
                                </NavLink>
                            </li>
                            <li className="welcome-module-list welcome-module-list-2">
                                <NavLink to="/login" className="welcome-module-button">
                                    Posting pekerjaan dan temukan orang yang tepat
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <img className="welcome-illustration-desktop img-fluid" alt="Selamat datang di Job Matching System" src="/img/Welcome.png"></img>
                    </div>
                </section>
                <section className="section-user row pt-5 pb-6 root-padding">
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="d-none d-lg-block">
                            <img className="img-fluid" alt="Pengguna JMS" src="/img/JMS User.png"></img>
                        </div>
                    </div>
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <h1 className="user-headline text-2">Siapa yang bisa menggunakan JMS?</h1>
                        <p className="user-module font-size-32">Mahasiswa dan alumni Jurusan Teknik Komputer dan Informatika Politeknik Negeri Bandung (JTK Polban)</p>
                    </div>
                </section>
            </div>
            <div className="container-fluid bg-1">
                <section className="section-workflow row pt-6 pb-5 root-padding">
                    <div className="section-workflow-left col-lg-6 d-flex align-content-center flex-wrap">
                        <h1 className="workflow-headline text-2">Alur kerja sistem</h1>
                    </div>
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="d-none d-lg-block">
                            <img className="img-fluid" alt="Alur Kerja Sistem" src="/img/Workflow.png"></img>
                        </div>
                    </div>
                </section>
                <section className="row pt-5 pb-5 root-padding">
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="d-none d-lg-block">
                            <img className="img-fluid" alt="Create Account" src="/img/Create Account.png"></img>
                        </div>
                    </div>
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="workflow-number col-lg-2">
                            <img className="img-number img-fluid" alt="1" src="/img/1.png"></img>
                        </div>
                        <div className="workflow-headline-module col">
                            <h2 className="text-2">Buat Akun Anda</h2>
                            <p className="workflow-module font-size-32">Buat akun Anda dengan mengisi data berupa email, kata sandi, dan data lainnya</p>
                        </div>
                    </div>
                </section>
                <section className="row pt-5 pb-5 root-padding">
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="d-none d-lg-block">
                            <img className="img-fluid" alt="Add Skill" src="/img/Add Skill.png"></img>
                        </div>
                    </div>
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="workflow-number col-lg-2">
                            <img className="img-number img-fluid" alt="2" src="/img/2.png"></img>
                        </div>
                        <div className="workflow-headline-module col">
                            <h2 className="text-2">Tambahkan Keahlian pada Profil atau Informasi Lowongan</h2>
                            <p className="workflow-module font-size-32">Cari dan pilih keahlian yang sesuai dengan keahlian Anda, atau keahlian yang dibutuhkan oleh lowongan yang Anda buat</p>
                        </div>
                    </div>
                </section>
                <section className="section-workflow-3 row pt-5 pb-6 root-padding">
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="d-none d-lg-block">
                            <img className="img-fluid" alt="Search Job or Select Applicant" src="/img/Search Job or Select Applicant.png"></img>
                        </div>
                    </div>
                    <div className="col-lg-6 d-flex align-content-center flex-wrap">
                        <div className="workflow-number col-lg-2">
                            <img className="img-number img-fluid" alt="3" src="/img/3.png"></img>
                        </div>
                        <div className="workflow-headline-module col">
                            <h2 className="text-2">Cari Lowongan Kerja  dan Proyek, atau Pilih Pelamar dengan Bantuan Hasil Rekomendasi</h2>
                            <p className="workflow-module font-size-32">Rekomendasi lowongan kerja dan proyek, atau pelamar didasarkan pada keahlian</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
  }
}
 
export default Home;