import React, { Component } from "react";
import {
  Route,
  NavLink,
  withRouter,
  Switch
} from "react-router-dom";
import Home from "./Home";
import Login from "./Login";
import Job from "./Job/Job";
import PostedJob from "./PostedJob/PostedJob";
import PostedJobDetail from "./PostedJob/PostedJobDetail";
import Profile from "./Profile";
import JobDetail from "./Job/JobDetail";
import ReactRoundedImage from "react-rounded-image";
import SearchedJobs from "./Job/SearchedJobs";
import EditPostedJob from "./PostedJob/EditPostedJob";
import PostJob from "./Job/PostJob";
import Signup from "./Signup";
import Verification from "./Verification"
import AddPersonalIdentity from "./PersonalIdentity/AddPersonalIdentity";

function setUser(user) {
    sessionStorage.setItem('nim', user.nim);
    sessionStorage.setItem('name', user.name);
    sessionStorage.setItem('token', user.accessToken);
    sessionStorage.setItem('photo', user.photo);
}

function getNim() {
    const nim = sessionStorage.getItem('nim');
    return nim
}

function getPhoto() {
    const photo = sessionStorage.getItem('photo');
    return photo
}

function getName() {
    const name = sessionStorage.getItem('name');
    return name
}

function getToken() {
    const token = sessionStorage.getItem('token');
    return token
}

class Main extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            isLoaded: false
        }
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        const name = getName();
        const token = getToken();

        if(token) {
            if (this.props.location.pathname === "/" || this.props.location.pathname === "/login" || this.props.location.pathname === "/signup" || this.props.location.pathname === "/verification" || this.props.location.pathname === "/new-profile") {
                if (name) {
                    this.props.history.push('/jobs');
                } else {
                    this.logout();
                }
            } else {
                if (name) {
                    this.props.history.push(this.props.location.pathname);
                } else {
                    this.logout();
                }
            }
        } else if (this.props.location.pathname !== '/login') {
            this.props.history.push('/login');
        }
    }

    logout() {
        sessionStorage.clear();
        this.props.history.push('/login');
    }

    render() {
        const nim = getNim();
        const photo = getPhoto();
        const name = getName();
        const token = getToken();

        return (
                <div className="application-container d-flex flex-column h-100">
                    { this.props.location.pathname !== '/login' && this.props.location.pathname !== '/signup' && this.props.location.pathname !== '/verification' && this.props.location.pathname !== '/new-profile'?
                    <nav className="navbar navbar-expand-lg root-padding bg-white py-2-5 align-items-center flex-column">
                        <div className="row w-100">
                            <div className="col-lg-4 col-4">
                                { token?
                                    <NavLink to="/jobs" className="navbar-brand brand font-size-58 text-1"><b>JMS</b></NavLink>
                                :
                                    <NavLink to="/" className="navbar-brand brand font-size-58 text-1"><b>JMS</b></NavLink>
                                }
                            </div>
                            <div className="col-lg-8 col-8 navbar-cta navbar-collapse">
                                { this.props.location.pathname === '/' ?
                                    <ul className="navbar-nav nav ml-auto">
                                        <li className="nav-item mr-2-5">
                                            <NavLink to="/signup" className="nav-link text-3"><b>Bergabung sekarang</b></NavLink>
                                        </li>
                                        <li className="nav-item">
                                            <NavLink to="/login" className="nav-login rounded-24 border-1 nav-link px-4 text-1"><b>Login</b></NavLink>
                                        </li>
                                    </ul>
                                :
                                    <ul className="navbar-nav nav h-100 align-items-center ml-auto">
                                        <div className="d-none d-lg-block">
                                            <li className="nav-item d-flex mr-4">
                                                <NavLink to="/jobs" className="nav-jobs d-flex align-items-center">
                                                    { this.props.location.pathname === '/jobs' ?
                                                        <img className="nav-my-photo" src="/img/Job Active.svg" alt="Me" width="24" height="24"></img>
                                                    :
                                                        <img className="nav-my-photo" src="/img/Job Passive.svg" alt="Me" width="24" height="24"></img>
                                                    }
                                                    { this.props.location.pathname === '/jobs' ?
                                                        <div className="nav-link-text text-black font-size-15">
                                                            Pekerjaan
                                                        </div>
                                                    :
                                                        <div className="text-3 font-size-15">
                                                            Pekerjaan
                                                        </div>
                                                    }
                                                </NavLink>
                                            </li>
                                        </div>
                                        <div className="icon nav-item btn-group d-none d-lg-block">
                                            <button className="nav-me d-flex align-items-center btn p-0 dropdown-toggle" id="dropdownMenuMe" type="button" data-toggle="dropdown" aria-expanded="false">
                                                <ReactRoundedImage image={photo !== "undefined"? photo : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="24" imageHeight="24" roundedSize="0" className="nav-my-photo" alt="Me"/>
                                                <div>
                                                    <div className="text-3 font-size-15">
                                                        Saya
                                                        <img className="nav-icon ml-1" src="/img/Down Arrow.svg" alt="Down Arrow"></img>
                                                    </div>
                                                </div>
                                            </button>
                                            <ul className="dropdown-menu dropdown-menu-right dropdown-menu-me mt-4">
                                                <div className="px-2">
                                                    <NavLink to={{pathname: `/profile/${nim}`}} className="dropdown-profile d-flex align-items-center text-black font-weight-semi-bold text-decoration-none">
                                                        <ReactRoundedImage image={photo !== "undefined"? photo : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="48" imageHeight="48" roundedSize="0" className="nav-my-photo-dropdown" alt="Me"/>
                                                        <p className="ml-2 mb-0">{name}</p>
                                                    </NavLink>
                                                    <NavLink to={{pathname: `/profile/${nim}`}} className="nav-profile nav-link mt-2 border-2 rounded-32 p-1 text-center text-1 font-weight-bold" id="nav-profile">Lihat Profil</NavLink>
                                                </div>
                                                <hr className="mt-2 mb-0"></hr>
                                                <li>
                                                    <button className="dropdown-item px-2 text-center" type="button" onClick={this.logout}>Logout</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="icon nav-item btn-group d-lg-none ml-auto">
                                            <button className="nav-me d-flex align-items-center btn p-0 navbar-toggler" id="dropdownMenuMe" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toogle Navigation">
                                                <ReactRoundedImage image={photo !== "undefined"? photo : "/img/Me.svg"} roundedColor="#FFFFFF" imageWidth="24" imageHeight="24" roundedSize="0" className="nav-my-photo" alt={name}/>
                                                <div className="d-none d-lg-block">
                                                    <div className="text-3 font-size-15">
                                                        Saya
                                                        <img className="nav-icon ml-1" src="/img/Down Arrow.svg" alt="Down Arrow"></img>
                                                    </div>
                                                </div>
                                            </button>
                                            
                                        </div>
                                        
                                    </ul>
                                }
                            </div>
                        </div>
                        <div className="d-lg-none w-100">
                            <div className="row collapse navbar-collapse mt-4 " id="navbarSupportedContent">
                                <div className="col-lg-12">
                                    <div className="px-2 text-center">
                                        <b>{name}</b>
                                        <NavLink to={{pathname: `/profile/${nim}`}} className="nav-profile nav-link mt-2 border-2 rounded-32 p-1 text-center text-1 font-weight-bold" id="nav-profile">Lihat Profil</NavLink>
                                    </div>
                                    <hr className="mt-3 mb-0"></hr>
                                    <ul className="mt-2 mb-0">
                                        <li>
                                            <button className="dropdown-item px-2 text-center" type="button" onClick={this.logout}>Logout</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>  
                    </nav>
                    : null}
                    { this.props.location.pathname !== '/' && this.props.location.pathname !== '/login' && this.props.location.pathname !== '/signup' && this.props.location.pathname !== '/verification' && this.props.location.pathname !== '/new-profile'?
                        <nav className="d-lg-none">
                            <ol className="nav-v2 w-100 nav justify-content-center">
                                <li className="nav-item">
                                    <NavLink to="/jobs" className="nav-jobs nav-link d-flex align-items-center" id="nav-jobs-small">
                                        { this.props.location.pathname === '/jobs' ?
                                            <img className="nav-my-photo" src="/img/Job Active.svg" alt="Me" width="24" height="24"></img>
                                                :
                                            <img className="nav-my-photo" src="/img/Job Passive.svg" alt="Me" width="24" height="24"></img>
                                        }
                                        { this.props.location.pathname === '/jobs' ?
                                            <div className="nav-icon-content nav-link-text text-black font-size-11">
                                                Pekerjaan
                                            </div>
                                        :
                                            <div className="nav-icon-content text-3 font-size-11">
                                                Pekerjaan
                                            </div>
                                        }
                                    </NavLink>
                                </li>
                            </ol>
                        </nav>
                    :
                        null
                    }
                    <div className="content d-flex flex-column h-100 flex-grow-1">
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/login" component={() => <Login setUser={setUser} />}/>
                        <Route exact path="/signup" component={() => <Signup />}/>
                        <Route exact path="/verification" component={() => <Verification setUser={setUser}/>}/>
                        <Route exact path="/new-profile" component={() => <AddPersonalIdentity getToken={getToken} getNim={getNim} setUser={setUser}/>}/>
                        <Route exact path="/jobs" component={() => <Job getToken={getToken} />}/>
                        <Route exact path="/searched-jobs/:keyword" component={(props) => <SearchedJobs {...props} getToken={getToken} />}/>
                        <Route exact path="/post-job" component={() => <PostJob getToken={getToken}/>}/>
                        <Route exact path="/posted-jobs" component={() => <PostedJob getNim={getNim} getToken={getToken}/>}/>
                        <Route exact path="/posted-job-detail/:jobId" component={(props) => <PostedJobDetail {...props} getToken={getToken}/>}/>
                        <Route exact path="/edit-posted-job/:id" component={(props) => <EditPostedJob {...props} getToken={getToken}/>}/>
                        {/* <Route exact path="/profile" component={() => <Profile getToken={getToken}/>}/> */}
                        <Route exact path="/jobs/:jobId" component={(props) => <JobDetail {...props} getToken={getToken}/>}/>
                        <Route exact path="/profile/:userId" component={(props) => <Profile {...props} getToken={getToken} getNim={getNim}/>}/>
                    </Switch>
                    </div>  
                    <div className="d-none d-lg-block">
                        <footer className="w-100 root-padding d-flex justify-content-center flex-wrap font-size-12">
                            {this.props.location.pathname === "/signup"?
                                <span className="text-black"><b>JMS</b></span>
                            :
                                <span className="text-1"><b>JMS</b></span>
                            }
                            
                            <span className="footer-copy ml-2">© 2021</span>
                        </footer>
                    </div>
                </div >
        );
    }
}

export default withRouter(Main);